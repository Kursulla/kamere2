package com.eutechpro.android.stanje.roadworks;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;

import com.eutechpro.android.stanje.commons.db.DbHelper;
import com.eutechpro.android.stanje.data.entities.MapPoint;

import org.junit.After;
import org.robolectric.RuntimeEnvironment;

public abstract class RoadworksDbTestSuite {
    protected static final int    ID_1          = 111;
    protected static final String TITLE_1       = "title GP 1";
    protected static final String DESCRIPTION_1 = "description_1";
    protected static final String LAT_1         = "lat_1";
    protected static final String LNG_1         = "lng_1";
    protected static final String META_1        = "meta_1";
    protected static final String TYPE_1        = "type_1";
    protected static final int    ID_2          = 222;
    protected static final String TITLE_2       = "title_2";
    protected static final String DESCRIPTION_2 = "description_2";
    protected static final String LAT_2         = "lat_2";
    protected static final String LNG_2         = "lng_2";
    protected static final String META_2        = "meta_2";
    protected static final String TYPE_2        = "type_2";
    protected DbHelper dbHelper;
    private   MapPoint mapPoint1;
    private   MapPoint mapPoint2;
    private   String   databaseName;

    protected void initRoadworksValuesInDb(String databaseName) {
        this.databaseName = databaseName;
        dbHelper = new DbHelper(RuntimeEnvironment.application, this.databaseName, 1);

        mapPoint1 = new MapPoint();
        mapPoint1.setId(ID_1);
        mapPoint1.setTitle(TITLE_1);
        mapPoint1.setDescription(DESCRIPTION_1);
        mapPoint1.setLat(LAT_1);
        mapPoint1.setLng(LNG_1);
        mapPoint1.setMeta(META_1);
        mapPoint1.setType(TYPE_1);

        mapPoint2 = new MapPoint();
        mapPoint2.setId(ID_2);
        mapPoint2.setTitle(TITLE_2);
        mapPoint2.setDescription(DESCRIPTION_2);
        mapPoint2.setLat(LAT_2);
        mapPoint2.setLng(LNG_2);
        mapPoint2.setMeta(META_2);
        mapPoint2.setType(TYPE_2);
    }

    protected MapPoint getPreparedRoadworkEntity1() {
        return mapPoint1;
    }

    protected MapPoint getPreparedRoadworkEntity2() {
        return mapPoint2;
    }

    protected void insertRoadworkIntoDatabase(MapPoint mapPoint) {
        SQLiteDatabase database      = dbHelper.getWritableDatabase();
        ContentValues  contentValues = DbHelper.roadworksModelToContentValues(mapPoint);
        database.insertWithOnConflict(DbHelper.ROADWORKS_TABLE_NAME, null, contentValues, SQLiteDatabase.CONFLICT_REPLACE);
    }

    @After
    public void tearDown() {
        dbHelper.close();
        RuntimeEnvironment.application.deleteDatabase(databaseName);
    }
}
