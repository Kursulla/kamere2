package com.eutechpro.android.stanje.splash;

import com.eutechpro.android.stanje.TestConsumer;
import com.eutechpro.android.stanje.cameras.CamerasRepository;
import com.eutechpro.android.stanje.cameras.CamerasRestApi;
import com.eutechpro.android.stanje.commons.RefreshTracker;
import com.eutechpro.android.stanje.data.entities.MapPoint;
import com.eutechpro.android.stanje.roadworks.RoadworksRepository;
import com.eutechpro.android.stanje.roadworks.RoadworksRestApi;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.robolectric.shadows.ShadowLog;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class MvpModelTest{
    @Mock
    private CamerasRestApi      camerasRestApi;
    @Mock
    private CamerasRepository   camerasRepository;
    @Mock
    private RoadworksRestApi    roadworksRestApi;
    @Mock
    private RoadworksRepository roadworksRepository;
    @Mock
    private RefreshTracker      refreshTracker;

    private Mvp.Model mvpModel;//SUT

    private List<MapPoint>                        points;
    private TestConsumer<MvpModel.FetchingResult> testSubscriber;


    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        ShadowLog.stream = System.out;

        MapPoint mapPoint = new MapPoint(1, "title", "lat", "lng");
        points = new ArrayList<>();
        points.add(mapPoint);

//        when(camerasRepository.removeAllCameras()).thenReturn(Observable.just(true));
//        when(roadworksRepository.removeAllRoadworksRx()).thenReturn(Observable.just(true));

        mvpModel = new MvpModel(camerasRestApi, camerasRepository, roadworksRestApi, roadworksRepository, refreshTracker);

        testSubscriber = new TestConsumer<>();
        mvpModel.dataStream().subscribe(testSubscriber);
    }

    @Test
    public void testRefreshingAndPushingDownTheStream() throws Exception {
        //Given
        when(camerasRestApi.downloadCameras()).thenReturn(Observable.just(points));
        when(roadworksRestApi.downloadAllRoadworks()).thenReturn(Observable.just(points));

        //When
        mvpModel.refreshData();

        //Then
        testSubscriber.assertNoErrors();
        verify(camerasRestApi).downloadCameras();
        verify(camerasRepository).replaceAllCameras(points);


        verify(roadworksRestApi).downloadAllRoadworks();
        verify(roadworksRepository).replaceAllRoadworks(points);
    }
}