package com.eutechpro.android.stanje.fines;

import com.eutechpro.android.stanje.TestConsumer;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;

import static junit.framework.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


public class FinesModelTest {
    private FinesRepository repository;
    private List<Fine>      listOfAllFines;
    private Mvp.Model       model;

    @Before
    public void setUp() throws Exception {
        Fine f1 = mock(Fine.class);
        when(f1.getDescription()).thenReturn("red light");
        Fine f2 = mock(Fine.class);
        when(f2.getDescription()).thenReturn("speed 30km");
        Fine f3 = mock(Fine.class);
        when(f3.getDescription()).thenReturn("alchocol in blood");

        listOfAllFines = new ArrayList<>();
        listOfAllFines.add(f1);
        listOfAllFines.add(f2);
        listOfAllFines.add(f3);

        repository = mock(FinesRepository.class);
        when(repository.getAllFines()).thenReturn(Observable.just(listOfAllFines));
        model = new MvpModel(repository);
    }

    @Test
    public void test_fetching_fines_by_query_empty() throws Exception {
        TestConsumer<List<Fine>> testSubscriber = new TestConsumer<>();

        model.getFinesByQuery("").subscribe(testSubscriber);

        testSubscriber.assertNoErrors();
        testSubscriber.assertComplete();
        assertEquals(3, testSubscriber.getContent().size());
    }

    @Test
    public void test_fetching_fines_by_non_empty_query() throws Exception {
        TestConsumer<List<Fine>> testSubscriber = new TestConsumer<>();

        model.getFinesByQuery("red").subscribe(testSubscriber);

        testSubscriber.assertNoErrors();
        testSubscriber.assertComplete();
        assertEquals(1, testSubscriber.getContent().size());
        assertEquals("red light", testSubscriber.getContent().get(0).getDescription());
    }
}