package com.eutechpro.android.stanje;

import android.app.Application;

import com.eutechpro.android.stanje.border.BorderDiComponent;
import com.eutechpro.android.stanje.cameras.CamerasDiComponent;
import com.eutechpro.android.stanje.roadworks.RoadworksDiComponent;
import com.eutechpro.android.stanje.splash.SplashScreenDiComponent;

public class MockKamereApplication extends Application {
    private static CamerasDiComponent      camerasActivityComponent;
    private static RoadworksDiComponent    roadworksDiComponent;
    private static BorderDiComponent       borderDiComponent;
    private static SplashScreenDiComponent splashScreenComponent;

    @Override
    public void onCreate() {
        super.onCreate();
    }

    public static RoadworksDiComponent getRoadworksDiComponent() {
        return roadworksDiComponent;
    }

    public static BorderDiComponent getBorderDiComponent() {
        return borderDiComponent;
    }

    public static CamerasDiComponent getCamerasActivityComponent() {
        return camerasActivityComponent;
    }

    public static SplashScreenDiComponent getSplashScreenComponent() {
        return splashScreenComponent;
    }
}
