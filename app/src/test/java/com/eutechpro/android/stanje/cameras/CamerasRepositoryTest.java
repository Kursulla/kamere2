package com.eutechpro.android.stanje.cameras;

import com.eutechpro.android.stanje.BuildConfig;
import com.eutechpro.android.stanje.TestConsumer;
import com.eutechpro.android.stanje.data.entities.MapPoint;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;

@RunWith(RobolectricTestRunner.class)
@Config(
        constants = BuildConfig.class,
        sdk = 21,
        manifest = Config.NONE
)
public class CamerasRepositoryTest extends CamerasDbTestSuite {
    private static final String DB_NAME = "test_database_name_cameras";
    private CamerasRepository repository;


    @Before
    public void setUp() throws Exception {
        super.initCamerasValuesInDb(DB_NAME);
        repository = new CamerasRepositoryDb(dbHelper);
    }

    @Test
    public void testSavingSingleCameraToDb() throws Exception {
        //Given
        MapPoint              roadwork       = getPreparedCameraEntity1();
        TestConsumer<Boolean> testSubscriber = new TestConsumer<>();

        //When
        repository.saveCameraPoint(roadwork).subscribe(testSubscriber);
        testSubscriber.awaitTerminalEvent(1000, TimeUnit.MILLISECONDS);

        //Then
        testSubscriber.assertComplete();
        testSubscriber.assertNoErrors();
//        List<Boolean> expectedResult = new ArrayList<>();
//        expectedResult.add(true);
        testSubscriber.assertResult(true);

    }

    @Test
    public void testSavingMultipleCamerasToDb() throws Exception {
        //Given
        MapPoint       camera1 = getPreparedCameraEntity1();
        MapPoint       camera2 = getPreparedCameraEntity2();
        List<MapPoint> cameras = new ArrayList<>(2);
        cameras.add(camera1);
        cameras.add(camera2);

        TestConsumer<Boolean> testSubscriber = new TestConsumer<>();

        //When
        repository.saveCamerasInBulk(cameras).subscribe(testSubscriber);
        testSubscriber.awaitTerminalEvent(1000, TimeUnit.MILLISECONDS);

        //Then
        testSubscriber.assertComplete();
        testSubscriber.assertNoErrors();
        List<Boolean> expectedResult = new ArrayList<>();
        expectedResult.add(true);
        testSubscriber.assertResult(true);
    }

    @Test
    public void testFetchingCamerasFromDb() throws Exception {
        //Given
        insertCameraIntoDatabase(getPreparedCameraEntity1());
        insertCameraIntoDatabase(getPreparedCameraEntity2());
        TestConsumer<List<MapPoint>> testSubscriber = new TestConsumer<>();

        //When
        repository.getAllCameras().subscribe(testSubscriber);
        testSubscriber.awaitTerminalEvent(1000, TimeUnit.MILLISECONDS);

        //Then
        testSubscriber.assertNoErrors();
        testSubscriber.assertComplete();
        List<MapPoint> camerasFromDb = testSubscriber.getContent();

        assertEquals(2, camerasFromDb.size());

        MapPoint camera1 = camerasFromDb.get(0);

        Assert.assertEquals(TITLE_1, camera1.getTitle());
        Assert.assertEquals(DESCRIPTION_1, camera1.getDescription());
        Assert.assertEquals(LAT_1, camera1.getLat());
        Assert.assertEquals(LNG_1, camera1.getLng());
        Assert.assertEquals(META_1, camera1.getMeta());
        Assert.assertEquals(TYPE_1, camera1.getType());

        MapPoint camera2 = camerasFromDb.get(1);
        Assert.assertEquals(TITLE_2, camera2.getTitle());
        Assert.assertEquals(DESCRIPTION_2, camera2.getDescription());
        Assert.assertEquals(LAT_2, camera2.getLat());
        Assert.assertEquals(LNG_2, camera2.getLng());
        Assert.assertEquals(META_2, camera2.getMeta());
        Assert.assertEquals(TYPE_2, camera2.getType());
    }
}