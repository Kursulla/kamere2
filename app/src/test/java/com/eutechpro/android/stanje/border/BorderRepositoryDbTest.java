package com.eutechpro.android.stanje.border;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;

import com.eutechpro.android.stanje.BuildConfig;
import com.eutechpro.android.stanje.MockKamereApplication;
import com.eutechpro.android.stanje.TestConsumer;
import com.eutechpro.android.stanje.commons.db.DbHelper;
import com.eutechpro.android.stanje.data.entities.MapPoint;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import java.util.List;
import java.util.concurrent.TimeUnit;

import static junit.framework.Assert.assertEquals;

@RunWith(RobolectricTestRunner.class)
@Config(
        constants = BuildConfig.class,
        application = MockKamereApplication.class,
        sdk = 21,
        manifest = "src/main/AndroidManifest.xml")
public class BorderRepositoryDbTest {
    private static final String DB_NAME = "test_database_name_border";

    private static final int    ID_1          = 111;
    private static final String TITLE_1       = "title GP 1";
    private static final String DESCRIPTION_1 = "description_1";
    private static final String LAT_1         = "lat_1";
    private static final String LNG_1         = "lng_1";
    private static final String META_1        = "meta_1";
    private static final String TYPE_1        = "type_1";
    private static final int    ID_2          = 222;
    private static final String TITLE_2       = "title_2";
    private static final String DESCRIPTION_2 = "description_2";
    private static final String LAT_2         = "lat_2";
    private static final String LNG_2         = "lng_2";
    private static final String META_2        = "meta_2";
    private static final String TYPE_2        = "type_2";

    private BorderRepositoryDb repository;
    private DbHelper           dbHelper;

    @Before
    public void setUp() throws Exception {
        dbHelper = new DbHelper(RuntimeEnvironment.application, DB_NAME, 1);

        repository = new BorderRepositoryDb(dbHelper);

        MapPoint bc1 = new MapPoint();
        bc1.setId(ID_1);
        bc1.setTitle(TITLE_1);
        bc1.setDescription(DESCRIPTION_1);
        bc1.setLat(LAT_1);
        bc1.setLng(LNG_1);
        bc1.setMeta(META_1);
        bc1.setType(TYPE_1);

        MapPoint bc2 = new MapPoint();
        bc2.setId(ID_2);
        bc2.setTitle(TITLE_2);
        bc2.setDescription(DESCRIPTION_2);
        bc2.setLat(LAT_2);
        bc2.setLng(LNG_2);
        bc2.setMeta(META_2);
        bc2.setType(TYPE_2);

        SQLiteDatabase database      = dbHelper.getWritableDatabase();
        ContentValues  contentValues = DbHelper.roadworksModelToContentValues(bc1);
        database.insertWithOnConflict(DbHelper.ROADWORKS_TABLE_NAME, null, contentValues, SQLiteDatabase.CONFLICT_REPLACE);

        contentValues = DbHelper.roadworksModelToContentValues(bc2);
        database.insertWithOnConflict(DbHelper.ROADWORKS_TABLE_NAME, null, contentValues, SQLiteDatabase.CONFLICT_REPLACE);
    }

    @Test
    public void getBorderCrossingsFromDbRepository() throws Exception {
        TestConsumer<List<MapPoint>> testConsumer = new TestConsumer<List<MapPoint>>();
        repository.getBorderCrossings().subscribe(testConsumer);
        testConsumer.awaitTerminalEvent(1000, TimeUnit.MILLISECONDS);
        testConsumer.assertNoErrors();
        testConsumer.assertComplete();
        List<MapPoint> crossings = testConsumer.getContent();
        assertEquals(1, crossings.size());

        MapPoint bc1 = crossings.get(0);

        assertEquals(ID_1, bc1.getId());
        assertEquals(TITLE_1, bc1.getTitle());
        assertEquals(DESCRIPTION_1, bc1.getDescription());
        assertEquals(LAT_1, bc1.getLat());
        assertEquals(LNG_1, bc1.getLng());
        assertEquals(META_1, bc1.getMeta());
        assertEquals(TYPE_1, bc1.getType());
    }

    @After
    public void tearDown() throws Exception {
        dbHelper.close();
        RuntimeEnvironment.application.deleteDatabase(DB_NAME);
    }
}