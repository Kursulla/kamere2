package com.eutechpro.android.stanje.fines;

import android.app.Activity;
import android.content.res.AssetManager;

import com.eutechpro.android.stanje.BuildConfig;
import com.eutechpro.android.stanje.DevActivity;
import com.eutechpro.android.stanje.MockKamereApplication;
import com.eutechpro.android.stanje.TestConsumer;
import com.eutechpro.android.stanje.commons.AssetsReader;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;
import org.robolectric.util.ActivityController;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class,
        application = MockKamereApplication.class,
        sdk = 21)
public class FinesAssetRepositoryTest {
    private static final String FINES_MOCK_JSON_FILE = "fines/kazne_mock.json";
    private AssetManager assetManager;

    @Before
    public void setUp() throws Exception {
        ActivityController<DevActivity> controller = Robolectric.buildActivity(DevActivity.class);
        Activity activity = controller.get();

        assetManager = activity.getAssets();
    }

    @Test
    public void testGetAllFines() throws Exception {
        FinesRepository repository = new FinesAssetRepository(new AssetsReader(assetManager), FINES_MOCK_JSON_FILE);

        TestConsumer<List<Fine>> testSubscriber = new TestConsumer<>();
        repository.getAllFines().subscribe(testSubscriber);
        testSubscriber.assertNoErrors();
        testSubscriber.assertComplete();

        List<Fine> fines = testSubscriber.getContent();

        assertNotNull("Fines parsed from assets directory failed. It returns null",fines);
        assertEquals("Wrong number of parsed fines.", 2, fines.size());
        assertEquals("Wrong value for \"description\" is wrong.", "Mock description 1", fines.get(0).getDescription());
        assertEquals("Wrong value for \"light points\" is wrong.", 10, fines.get(0).getLightPoints());
        assertEquals("Wrong value for \"light fine\" is wrong.", "Mock fine light 1", fines.get(0).getLightFine());
        assertEquals("Wrong value for \"light restrictions\" is wrong.", "Ne", fines.get(0).getLightRestriction());
        assertEquals("Wrong value for \"hard points\" is wrong.",11 , fines.get(0).getHardPoints());
        assertEquals("Wrong value for \"hard fine\" is wrong.", "1000", fines.get(0).getHardFine());
        assertEquals("Wrong value for \"hard restrictions\" is wrong.", "Mock 1 mesec", fines.get(0).getHardRestriction());
    }
    private class TestAssetsManager extends AssetsReader{

        public TestAssetsManager() {
            super(null);
        }

        @Override
        public String readAssetFrom(String path) {

            return "";
        }
    }
}