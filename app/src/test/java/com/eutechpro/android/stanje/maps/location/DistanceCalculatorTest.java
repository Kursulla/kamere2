package com.eutechpro.android.stanje.maps.location;

import android.location.Location;

import com.eutechpro.android.stanje.data.entities.MapPoint;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

public class DistanceCalculatorTest {

    private DistanceCalculator distanceCalculator;
    @Mock
    private Location           userLocation;
    private List<MapPoint>     cameras;
    private MapPoint           camera1;
    private MapPoint           camera2;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        distanceCalculator = new DistanceCalculator(400);

        //user 52.519430, 13.424778
        when(userLocation.getLatitude()).thenReturn(44.82077);
        when(userLocation.getLongitude()).thenReturn(20.49038);

//        when(userLocation.getLatitude()).thenReturn(44.82077);
//        when(userLocation.getLongitude()).thenReturn(20.49038);

        //camera 2  52.5227467,13.4063889
        cameras = new ArrayList<>(2);
//        camera1 = mock(MapPoint.class);
//        when(camera1.getLatDouble()).thenReturn(44.81862);
//        when(camera1.getLngDouble()).thenReturn(20.48968);

        camera1 = new MapPoint();
        camera1.setLat("44.81862");
        camera1.setLng("20.48968");

        //camera 1  52.518229, 13.429778
//        camera2 = mock(MapPoint.class);
//        when(camera2.getLatDouble()).thenReturn(44.81813);
//        when(camera2.getLngDouble()).thenReturn(20.48922);

        cameras.add(camera1);
//        cameras.add(camera2);

    }

    @Test
    public void testFindNearestCamera() throws Exception {
        when(userLocation.getLatitude()).thenReturn(44.82077);
        when(userLocation.getLongitude()).thenReturn(20.49038);

        boolean isSuccess = distanceCalculator.findNearestCamera(userLocation, cameras);


        assertTrue("Unable to find camera in close distance from user's location!", isSuccess);

        assertNotNull("Nearest camera found, but it is null which is not what we accept!", distanceCalculator.getNearestCamera());

        assertEquals("Nearest camera found, but not the one we expected", camera1, distanceCalculator.getNearestCamera());
    }

    @Test
    public void testGetDistanceToNearestCamera() throws Exception {
        when(userLocation.getLatitude()).thenReturn(44.82077);
        when(userLocation.getLongitude()).thenReturn(20.49038);

        boolean isSuccess = distanceCalculator.findNearestCamera(userLocation, cameras);

        assertTrue("Unable to find camera in close distance from user's location!", isSuccess);

        int distance = distanceCalculator.getDistanceToNearestCamera();

        assertEquals(245, distance);
    }
}