package com.eutechpro.android.stanje.cameras;

import com.eutechpro.android.stanje.data.entities.MapPoint;
import com.eutechpro.android.stanje.maps.location.SimpleLocationManager;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.android.plugins.RxAndroidPlugins;
import io.reactivex.schedulers.Schedulers;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyListOf;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class CamerasMvpPresenterTest {

    private Mvp.Presenter         presenter;
    private Mvp.Model             model;
    private Mvp.View              view;
    private SimpleLocationManager locationManager;

    @Before
    public void setUp() throws Exception {
        RxAndroidPlugins.setInitMainThreadSchedulerHandler(__ -> Schedulers.trampoline());
        List<MapPoint> cameras = new ArrayList<>();
        cameras.add(mock(MapPoint.class));

        model = mock(Mvp.Model.class);
        when(model.fetchCameras()).thenReturn(Observable.just(cameras));
        when(model.getCachedCameras()).thenReturn(cameras);

        view = mock(Mvp.View.class);

        locationManager = mock(SimpleLocationManager.class);
        presenter = new MvpPresenter(model, locationManager);

    }

    @Test
    public void bindView_OK() throws Exception {
        //Given

        //When
        presenter.bindView(view);

        //Then
        verify(model).fetchCameras();
        verify(model).cacheCameras(anyListOf(MapPoint.class));
    }

    @Test
    public void bindView_Error() throws Exception {
        //Given
        when(model.fetchCameras()).thenReturn(Observable.error(new Exception()));

        //When
        presenter.bindView(view);

        //Then
        verify(view).showError(anyInt());
    }

    @Test(expected = NullPointerException.class)
    public void unBindView() throws Exception {
        //Given
        presenter.bindView(view);

        //When
        presenter.unBindView();

        //Then
        presenter.tapOnMapMarker(new MapPoint());
    }

    @Test
    public void startLocationTracking() throws Exception {
        //Given
        presenter.bindView(view);

        //When
        presenter.startLocationTracking();

        //Then
        verify(model).getCachedCameras();
        verify(locationManager).connect();
        verify(locationManager).startLocationUpdates();
        verify(locationManager).setLocationChangesListener(any());
    }

    @Test
    public void stopLocationTracking() throws Exception {
        //Given
        presenter.bindView(view);

        //When
        presenter.stopLocationTracking();

        //Then
        verify(locationManager).stopLocationUpdates();
        verify(locationManager).disconnect();
        verify(view).hideWarningView();
    }

    @Test
    public void tapOnMapMarker() throws Exception {
        //Given
        presenter.bindView(view);

        //When
        presenter.tapOnMapMarker(new MapPoint());

        //Then
        verify(view).showCameraDetailsView(any(MapPoint.class));
    }

    @After
    public void tearDown() throws Exception {
        RxAndroidPlugins.reset();
    }
}