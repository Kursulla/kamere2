package com.eutechpro.android.stanje.report_camera;

import com.eutechpro.android.stanje.TestConsumer;

import org.junit.Before;
import org.junit.Test;

import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


public class ReportCameraModelTest {
    private UserCommunicationEndpoints                 apiEndpoints;
    private Mvp.Model                                  model;
    private UserCommunicationEndpoints.MessageResponse messageResponse;

    @Before
    public void setUp() throws Exception {
        apiEndpoints = mock(UserCommunicationEndpoints.class);
        UserCommunicationService service = new UserCommunicationService(apiEndpoints);
        model = new MvpModel(service);

        messageResponse = mock(UserCommunicationEndpoints.MessageResponse.class);
    }

    @Test
    public void report_camera_success() throws Exception {
        //Given
        when(messageResponse.getMessage()).thenReturn("success");
        when(apiEndpoints.reportCamera(anyString(), anyString())).thenReturn(Observable.just(messageResponse));
        TestConsumer<Boolean> testSubscriber = new TestConsumer<>();

        //When
        model.reportCamera(anyString(), anyString()).subscribe(testSubscriber);

        //Then
        testSubscriber.awaitTerminalEvent(3000, TimeUnit.MILLISECONDS);
        testSubscriber.assertNoErrors();
        testSubscriber.assertComplete();
        testSubscriber.assertResult(true);

    }

    @Test
    public void report_camera_fail() throws Exception {
        //Given
        when(messageResponse.getMessage()).thenReturn("fail");
        when(apiEndpoints.reportCamera(anyString(), anyString())).thenReturn(Observable.just(messageResponse));
        TestConsumer<Boolean> testSubscriber = new TestConsumer<>();

        //When
        model.reportCamera(anyString(), anyString()).subscribe(testSubscriber);

        //Then
        testSubscriber.awaitTerminalEvent(3000, TimeUnit.MILLISECONDS);
        testSubscriber.assertNoErrors();
        testSubscriber.assertComplete();
        testSubscriber.assertResult(false);
    }
}