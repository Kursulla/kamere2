package com.eutechpro.android.stanje.fines;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.Collections;
import java.util.List;
import io.reactivex.Observable;
import io.reactivex.android.plugins.RxAndroidPlugins;
import io.reactivex.schedulers.Schedulers;

import static org.mockito.Matchers.anyListOf;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


public class FinesPresenterTest {

    private Mvp.Presenter presenter;
    private Mvp.View      view;
    private Mvp.Model     model;

    @Before
    public void setUp() throws Exception {
        RxAndroidPlugins.setInitMainThreadSchedulerHandler(__ -> Schedulers.trampoline());
        view = mock(FinesActivity.class);
        model = mock(Mvp.Model.class);

        Observable<List<Fine>> fines = Observable.just(Collections.<Fine>emptyList());
        when(model.getFinesByQuery("")).thenReturn(fines);
    }

    @Test
    public void testPopulateView() throws Exception {

        presenter = new MvpPresenter(model);
        presenter.bindView(view);

        verify(view).showInitData(anyListOf(Fine.class));
    }
    @After
    public void tearDown() throws Exception {
        RxAndroidPlugins.reset();
    }
}