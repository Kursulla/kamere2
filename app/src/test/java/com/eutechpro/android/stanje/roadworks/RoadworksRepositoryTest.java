package com.eutechpro.android.stanje.roadworks;

import com.eutechpro.android.stanje.BuildConfig;
import com.eutechpro.android.stanje.TestConsumer;
import com.eutechpro.android.stanje.data.entities.MapPoint;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;

@RunWith(RobolectricTestRunner.class)
@Config(
        constants = BuildConfig.class,
        sdk = 21,
        manifest = Config.NONE
)
public class RoadworksRepositoryTest extends RoadworksDbTestSuite {
    private static final String DB_NAME = "test_database_name_roadworks";

    private RoadworksDbRepository repository;

    @Before
    public void setUp() throws Exception {
        super.initRoadworksValuesInDb(DB_NAME);
        repository = new RoadworksDbRepository(dbHelper);
    }

    @Test
    public void testSavingSingleRoadworkToDb() throws Exception {
        //Given
        MapPoint                roadwork       = getPreparedRoadworkEntity1();
        TestConsumer<Boolean> testSubscriber = new TestConsumer<>();

        //When
        repository.saveRoadworkRx(roadwork).subscribe(testSubscriber);
        testSubscriber.awaitTerminalEvent(1000, TimeUnit.MILLISECONDS);

        //Then
        testSubscriber.assertComplete();
        testSubscriber.assertNoErrors();
        List<Boolean> expectedResult = new ArrayList<>();
        expectedResult.add(true);
        testSubscriber.assertResult(true);
    }


    @Test
    public void testSavingMultipleRoadworksInDb() throws Exception {
        //Given
        MapPoint       roadwork1 = getPreparedRoadworkEntity1();
        MapPoint       roadwork2 = getPreparedRoadworkEntity2();
        List<MapPoint> roadworks = new ArrayList<>(2);
        roadworks.add(roadwork1);
        roadworks.add(roadwork2);

        TestConsumer<Boolean> testSubscriber = new TestConsumer<>();

        //When
        repository.saveRoadworksInBulkRx(roadworks).subscribe(testSubscriber);
        testSubscriber.awaitTerminalEvent(1000, TimeUnit.MILLISECONDS);

        //Then
        testSubscriber.assertComplete();
        testSubscriber.assertNoErrors();
        List<Boolean> expectedResult = new ArrayList<>();
        expectedResult.add(true);
        testSubscriber.assertResult(true);
    }

    @Test
    public void testFetchingRoadworksFromDb() throws Exception {
        //Given
        insertRoadworkIntoDatabase(super.getPreparedRoadworkEntity1());
        insertRoadworkIntoDatabase(super.getPreparedRoadworkEntity2());
        TestConsumer<List<MapPoint>> testSubscriber = new TestConsumer<>();

        //When
        repository.getRoadworksRx().subscribe(testSubscriber);
        testSubscriber.awaitTerminalEvent(1000, TimeUnit.MILLISECONDS);

        //Then
        testSubscriber.assertNoErrors();
        testSubscriber.assertComplete();
        List<MapPoint> roadworksFromDb = testSubscriber.getContent();

        assertEquals(2, roadworksFromDb.size());

        MapPoint roadWork1 = roadworksFromDb.get(0);

        Assert.assertEquals(ID_1, roadWork1.getId());
        Assert.assertEquals(TITLE_1, roadWork1.getTitle());
        Assert.assertEquals(DESCRIPTION_1, roadWork1.getDescription());
        Assert.assertEquals(LAT_1, roadWork1.getLat());
        Assert.assertEquals(LNG_1, roadWork1.getLng());
        Assert.assertEquals(META_1, roadWork1.getMeta());
        Assert.assertEquals(TYPE_1, roadWork1.getType());

        MapPoint roadWork2 = roadworksFromDb.get(1);
        Assert.assertEquals(ID_2, roadWork2.getId());
        Assert.assertEquals(TITLE_2, roadWork2.getTitle());
        Assert.assertEquals(DESCRIPTION_2, roadWork2.getDescription());
        Assert.assertEquals(LAT_2, roadWork2.getLat());
        Assert.assertEquals(LNG_2, roadWork2.getLng());
        Assert.assertEquals(META_2, roadWork2.getMeta());
        Assert.assertEquals(TYPE_2, roadWork2.getType());
    }
}