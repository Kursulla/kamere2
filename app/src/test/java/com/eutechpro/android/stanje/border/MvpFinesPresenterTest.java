package com.eutechpro.android.stanje.border;

import com.eutechpro.android.stanje.data.entities.MapPoint;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.android.plugins.RxAndroidPlugins;
import io.reactivex.schedulers.Schedulers;

import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyListOf;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
@RunWith(MockitoJUnitRunner.class)
public class MvpFinesPresenterTest {
    private Mvp.Model     model;
    private Mvp.View      view;
    private Mvp.Presenter presenter;
    @Mock
    private MapPoint      borderCrossing;
    private List<MapPoint> crossings;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        RxAndroidPlugins.setInitMainThreadSchedulerHandler(__ -> Schedulers.trampoline());

        crossings = new ArrayList<>();
        crossings.add(borderCrossing);

        model = mock(Mvp.Model.class);
        when(model.getBorderCrossing())
                .thenReturn(Observable.just(crossings));

        view = mock(Mvp.View.class);

        presenter = new MvpPresenter(model);
    }

    @Test
    public void tetBindView_OK() throws Exception {
        //Given


        //When
        presenter.bindView(view);

        //Then
        verify(model).getBorderCrossing();
        verify(view).drawMarkersOnMap(anyListOf(MapPoint.class));
    }

    @Test
    public void tetBindView_Error() throws Exception {
        //Given
        when(model.getBorderCrossing())
                .thenReturn(Observable.error(new Exception()));
        //When
        presenter.bindView(view);

        //Then
        verify(view).showError(anyInt());
    }

    @Test(expected = NullPointerException.class)
    public void unBindView() throws Exception {
        //Given
        presenter.bindView(view);

        //When
        presenter.unBindView();

        //Then
        presenter.tapOnMapMarker(borderCrossing);
    }

    @Test
    public void tapOnMapMarker() throws Exception {
        //Given
        presenter.bindView(view);

        //When
        presenter.tapOnMapMarker(borderCrossing);

        //Then
        verify(view).showRoadworkDetails(borderCrossing);
    }
    @After
    public void tearDown() throws Exception {
        RxAndroidPlugins.reset();
    }
}