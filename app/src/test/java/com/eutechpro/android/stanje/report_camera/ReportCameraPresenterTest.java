package com.eutechpro.android.stanje.report_camera;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import io.reactivex.Observable;
import io.reactivex.android.plugins.RxAndroidPlugins;
import io.reactivex.schedulers.Schedulers;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


public class ReportCameraPresenterTest {
    private Mvp.Presenter presenter;
    @Mock
    private Mvp.Model     model;
    @Mock
    private Mvp.View      view;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        RxAndroidPlugins.setInitMainThreadSchedulerHandler(__ -> Schedulers.trampoline());


        presenter = new MvpPresenter(model);
    }

    @Test
    public void sending_message_success() throws Exception {
        //Given
        when(model.reportCamera(anyString(), anyString())).thenReturn(Observable.just(true));
        presenter.bindView(view);

        //When
        presenter.sendMessage("123@asd.com", "324324");

        //Then
        verify(view).messageSent();
    }

    @Test
    public void sending_message_fail_with_exception() throws Exception {
        //Given
        when(model.reportCamera(anyString(), anyString())).thenReturn(Observable.error(new RuntimeException()));
        presenter.bindView(view);

        //When
        presenter.sendMessage("123@asd.com", "324324");

        //Then
        verify(view).messageFailed();
    }


    @Test
    public void sending_message_fail_with_status() throws Exception {
        //Given
        when(model.reportCamera(anyString(), anyString())).thenReturn(Observable.just(false));
        presenter.bindView(view);

        //When
        presenter.sendMessage("123@asd.com", "324324");

        //Then
        verify(view).messageFailed();
    }

    @Test(expected = IllegalStateException.class)
    public void call_before_binding_view() throws Exception {
        //Given
        when(model.reportCamera(anyString(), anyString())).thenReturn(Observable.just(true));

        //When
        presenter.sendMessage("123@asd.com", "324324");
    }
    @After
    public void tearDown() throws Exception {
        RxAndroidPlugins.reset();
    }
}