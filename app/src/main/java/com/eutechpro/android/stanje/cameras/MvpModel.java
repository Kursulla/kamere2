package com.eutechpro.android.stanje.cameras;

import android.support.annotation.NonNull;

import com.eutechpro.android.stanje.data.entities.MapPoint;

import java.util.Collections;
import java.util.List;

import io.reactivex.Observable;

class MvpModel implements Mvp.Model {
    private final CamerasRepository camerasRepository;
    private       List<MapPoint>    cachedCameras;

    MvpModel(CamerasRepository camerasRepository) {
        this.camerasRepository = camerasRepository;
        this.cachedCameras = Collections.emptyList();
    }

    @Override
    public Observable<List<MapPoint>> fetchCameras() {
        return camerasRepository.getAllCameras();
    }

    @NonNull
    @Override
    public List<MapPoint> getCachedCameras() {
        return cachedCameras;
    }

    @Override
    public void cacheCameras(@NonNull List<MapPoint> camerasToCache) {
        this.cachedCameras = camerasToCache;
    }
}
