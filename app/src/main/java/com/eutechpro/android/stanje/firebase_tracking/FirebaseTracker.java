package com.eutechpro.android.stanje.firebase_tracking;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;

import com.google.firebase.analytics.FirebaseAnalytics;

public class FirebaseTracker {
    FirebaseAnalytics fbAnalytics;

    public FirebaseTracker(Context context) {
        this.fbAnalytics = FirebaseAnalytics.getInstance(context);
    }

    public void logActivityOpenedFromDrawer(Class clazz) {
        Bundle bundle = new Bundle();
        bundle.putString("name", clazz.getSimpleName());
        fbAnalytics.logEvent("activity_opened", bundle);
    }

    public void logTrackingLength(long durationInMsSeconds) {
        Bundle bundle = new Bundle();
        bundle.putLong("duration", durationInMsSeconds);
        fbAnalytics.logEvent("tracking_warning_duration", bundle);
    }

    public void logWarningEvent(String eventTitle) {
        Bundle bundle = new Bundle();
        bundle.putString("tracking_warning_title", ellipsize(eventTitle));
        fbAnalytics.logEvent("tracking_warning_open", bundle);
    }

    public void logCameraDetailsOpening(String title) {
        Bundle bundle = new Bundle();
        bundle.putString("camera_details_title", ellipsize(title));
        fbAnalytics.logEvent("camera_details_open", bundle);
    }

    public void logRoadworksDetailsOpening(String title) {
        Bundle bundle = new Bundle();
        bundle.putString("roadworks_details_title", ellipsize(title));
        fbAnalytics.logEvent("roadworks_details_open", bundle);
    }

    public void logFineDetailsOpening(String description) {
        Bundle bundle = new Bundle();
        bundle.putString("fines_details_title", ellipsize(description));
        fbAnalytics.logEvent("fines_details_open", bundle);
    }

    public void logFinesFilteringQuery(String searchString) {
        Bundle bundle = new Bundle();
        bundle.putString("fines_filtering_query", ellipsize(searchString));
        fbAnalytics.logEvent("fines_search", bundle);
    }

    public void logSendingMessage(String email, String text) {
        Bundle bundle = new Bundle();
        bundle.putString("sending_message_email", ellipsize(email));
        bundle.putString("sending_message_text", ellipsize(text));
        fbAnalytics.logEvent("sending_message", bundle);
    }

    @NonNull
    private String ellipsize(String eventTitle) {
        return eventTitle.length() > 32 ? eventTitle.substring(0, 32) + "..." : eventTitle;
    }
}
