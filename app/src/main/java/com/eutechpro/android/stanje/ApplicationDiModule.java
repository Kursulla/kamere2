package com.eutechpro.android.stanje;

import android.content.Context;

import dagger.Module;
import dagger.Provides;

@Module
public class ApplicationDiModule {
    private Context context;

    ApplicationDiModule(Context context) {
        this.context = context;
    }


    @Provides
    Context provideApplicationContext() {
        return context;
    }
}
