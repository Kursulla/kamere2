package com.eutechpro.android.stanje.cameras;

import android.util.Log;

import com.eutechpro.android.stanje.R;
import com.eutechpro.android.stanje.data.entities.MapPoint;
import com.eutechpro.android.stanje.maps.location.DistanceCalculator;
import com.eutechpro.android.stanje.maps.location.SimpleLocationManager;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

class MvpPresenter implements Mvp.Presenter {
    private static final String TAG                = "CamerasMvpPresenter";
    private static final int    DISTANCE_THRESHOLD = 200;
    private final SimpleLocationManager locationManager;
    private final Mvp.Model             model;
    private final DistanceCalculator    distanceCalculator;
    private       Mvp.View              view;
    private       CompositeDisposable   subscriptions;
    private       List<Integer>         lastDistances;

    MvpPresenter(Mvp.Model model, SimpleLocationManager locationManager) {
        this.locationManager = locationManager;
        this.distanceCalculator = new DistanceCalculator(DISTANCE_THRESHOLD);
        this.model = model;
        this.subscriptions = new CompositeDisposable();
        this.lastDistances = new ArrayList<>();
    }

    @Override
    public void bindView(Mvp.View boundedView) {
        this.view = boundedView;
        Disposable subscription = model.fetchCameras()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        fetchedCameras -> {
                            model.cacheCameras(fetchedCameras);
                            view.drawMarkersOnMap(fetchedCameras);
                        },
                        throwable -> {
                            Log.e(TAG, "" + throwable.getLocalizedMessage());
                            view.showError(R.string.cameras_error_unable_to_fetch_cameras);
                });
        subscriptions.add(subscription);
    }

    @Override
    public void unBindView() {
        this.view = null;
        stopLocationTracking();
        subscriptions.clear();
    }

    @Override
    public void startLocationTracking() {
        initTracking(model.getCachedCameras());
    }

    @Override
    public void stopLocationTracking() {
        locationManager.stopLocationUpdates();
        locationManager.disconnect();
        if(view == null) {
            return;
        }
        view.hideWarningView();
    }

    private void initTracking(final List<MapPoint> camerasToTrack) {
        locationManager.connect();
        locationManager.startLocationUpdates();
        locationManager.setLocationChangesListener(usersLocation -> {
            if(view == null) {
                return;
            }
            view.focusMapToLocation(usersLocation);
            if(distanceCalculator.findNearestCamera(usersLocation, camerasToTrack)) {
                view.showWarningView(distanceCalculator.getNearestCamera(), distanceCalculator.getDistanceToNearestCamera());
            } else {
                view.hideWarningView();
            }
        });
    }

    private boolean isApproachingToTheCamera(int currentDistance){
        for(Integer distance: lastDistances){
            if(currentDistance > distance){

            }
        }
        return false;
    }

    @Override
    public void tapOnMapMarker(MapPoint mapPoint) {
        view.showCameraDetailsView(mapPoint);
    }

}
