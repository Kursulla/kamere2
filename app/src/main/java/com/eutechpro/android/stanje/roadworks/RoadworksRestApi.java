package com.eutechpro.android.stanje.roadworks;

import com.eutechpro.android.stanje.data.entities.MapPoint;

import java.util.List;

import io.reactivex.Observable;

/**
 * API for downloading roadworks information from server. REST API
 */
public interface RoadworksRestApi {
    Observable<List<MapPoint>> downloadAllRoadworks();
}
