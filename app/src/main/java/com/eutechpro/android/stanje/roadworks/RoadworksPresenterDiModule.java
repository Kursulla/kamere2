package com.eutechpro.android.stanje.roadworks;

import android.content.Context;

import com.eutechpro.android.stanje.commons.db.DbHelper;

import dagger.Provides;

@dagger.Module
public class RoadworksPresenterDiModule {
    @Provides
    Mvp.Presenter providesPresenter(Context context) {
        RoadworksRepository repository = new RoadworksDbRepository(DbHelper.getDbHelperInstance(context));
        return new MvpPresenter(new MvpModel(repository));
    }
}
