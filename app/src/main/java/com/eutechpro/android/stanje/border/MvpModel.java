package com.eutechpro.android.stanje.border;

import com.eutechpro.android.stanje.data.entities.MapPoint;

import java.util.List;

import io.reactivex.Observable;

class MvpModel implements Mvp.Model {
    private BordersRepository bordersRepository;

    MvpModel(BordersRepository bordersRepository) {
        this.bordersRepository = bordersRepository;
    }

    @Override
    public Observable<List<MapPoint>> getBorderCrossing() {
        return bordersRepository.getBorderCrossings();
    }
}
