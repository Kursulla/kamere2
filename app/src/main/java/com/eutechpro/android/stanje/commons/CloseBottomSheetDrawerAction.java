package com.eutechpro.android.stanje.commons;

import com.eutechpro.android.stanje.BaseActivity;

public class CloseBottomSheetDrawerAction implements BaseActivity.OnDrawerAction {
    private DetailsBottomSheet detailsBottomSheet;

    public CloseBottomSheetDrawerAction(DetailsBottomSheet detailsBottomSheet) {
        this.detailsBottomSheet = detailsBottomSheet;
    }

    @Override
    public void drawerOpened() {
        detailsBottomSheet.collapse();
    }

    @Override
    public void drawerClosed() {
        //unused
    }
}
