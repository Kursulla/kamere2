package com.eutechpro.android.stanje.roadworks;

import com.eutechpro.android.stanje.data.entities.MapPoint;
import com.eutechpro.android.stanje.data.rest.RetrofitFactory;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.http.GET;

/**
 * Retrofit implementation of {@link RoadworksRestApi}
 */
public class RoadworksRetrofitApiClient implements RoadworksRestApi {
    private RoadworksRetrofitInterface retrofitRestInterface;

    public RoadworksRetrofitApiClient() {
        retrofitRestInterface = RetrofitFactory.generateRetrofit().create(RoadworksRetrofitInterface.class);
    }

    @Override
    public Observable<List<MapPoint>> downloadAllRoadworks() {
        return retrofitRestInterface.downloadAllRoadworks()
                .flatMap(mapPoints -> {
                    if (mapPoints == null){
                        return Observable.error(new IllegalStateException("We ware unable to fetch new RoadWorks from server!!!"));
                    }
                    return Observable.just(mapPoints);
                }).subscribeOn(Schedulers.newThread());
    }

    /**
     * API interface for communicating with server.
     */
    interface RoadworksRetrofitInterface {
        @GET("roadworks")
        Observable<List<MapPoint>> downloadAllRoadworks();
    }
}
