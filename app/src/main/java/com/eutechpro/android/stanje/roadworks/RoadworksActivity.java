package com.eutechpro.android.stanje.roadworks;

import android.location.Location;
import android.os.Bundle;
import android.support.annotation.StringRes;
import android.util.Log;
import android.widget.Toast;

import com.eutechpro.android.stanje.BaseActivity;
import com.eutechpro.android.stanje.KamereApplication;
import com.eutechpro.android.stanje.R;
import com.eutechpro.android.stanje.commons.CloseBottomSheetDrawerAction;
import com.eutechpro.android.stanje.commons.DetailsBottomSheet;
import com.eutechpro.android.stanje.data.entities.MapPoint;
import com.eutechpro.android.stanje.firebase_tracking.FirebaseTracker;
import com.eutechpro.android.stanje.maps.GoogleMapManager;
import com.google.android.gms.maps.SupportMapFragment;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RoadworksActivity extends BaseActivity implements Mvp.View {
    private static final String TAG = "RoadworksActivity";
    @BindView(R.id.bottom_sheet)
    DetailsBottomSheet detailsBottomSheet;

    @Inject
    Mvp.Presenter      presenter;

    @Inject
    FirebaseTracker firebaseTracker;

    private GoogleMapManager mapManager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Inject
        KamereApplication.get(this).getRoadworksDiComponent().inject(this);

        // Setup layout
        setContentView(R.layout.roadworks_activity);
        ButterKnife.bind(this);
        super.initToolBarWithLeftDrawer(R.drawable.nav_roadworks_icon);
        super.initLeftDrawer();
        // Map
        mapManager = new GoogleMapManager(R.drawable.map_roadworks_marker);
        mapManager.attachTo((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map));
        mapManager.setOnMapInteractionListener(mapPoint -> {
            firebaseTracker.logRoadworksDetailsOpening(mapPoint.getTitle());
            presenter.tapOnMapMarker(mapPoint);
        });
        // Bottom sheet
        detailsBottomSheet.init();
        setOnDrawerAction(new CloseBottomSheetDrawerAction(detailsBottomSheet));
        hideBottomSheet();

        // Bind
        presenter.bindView(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.unBindView();
        hideBottomSheet();
    }

    @Override
    public void onBackPressed() {
        if(detailsBottomSheet.isExpanded()){
            detailsBottomSheet.collapse();
        }else{
            finish();
        }
    }

    @Override
    public void showBottomSheet(MapPoint mapPoint) {
        Log.d(TAG, "onMarkerClick: " + mapPoint.getTitle());
        detailsBottomSheet.setTitle(mapPoint.getTitle());
        detailsBottomSheet.setDescription(mapPoint.getDescription().replace("<br/>","\n"));
        detailsBottomSheet.expand();
    }

    @Override
    public void hideBottomSheet() {
        detailsBottomSheet.collapse();
    }


    @Override
    public void drawMarkersOnMap(List<MapPoint> cameras) {
        mapManager.clearMapMarkers();
        mapManager.drawMarkersOnMap(cameras);
    }

    @Override
    public void showError(@StringRes int errorMessageRes) {
        Toast.makeText(this, errorMessageRes, Toast.LENGTH_LONG).show();
    }

    @Override
    public void focusMapToLocation(Location location) {
        mapManager.moveToLocationAndKeepZoomLevel(location);
    }
}
