package com.eutechpro.android.stanje.report_camera;

import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.eutechpro.android.stanje.BaseActivity;
import com.eutechpro.android.stanje.KamereApplication;
import com.eutechpro.android.stanje.R;
import com.eutechpro.android.stanje.firebase_tracking.FirebaseTracker;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ReportCameraActivity extends BaseActivity implements Mvp.View {
    private static final Pattern VALID_EMAIL_ADDRESS_REGEX = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);

    @BindView(R.id.email)
    EditText        emailEt;
    @BindView(R.id.message)
    EditText        messageEt;
    @BindView(R.id.send_btn)
    Button          sendBtn;
    @Inject
    Mvp.Presenter   presenter;
    @Inject
    FirebaseTracker firebaseTracker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Inject
        KamereApplication.get(this).getReportCameraComponent().inject(this);
        //Set layout
        setContentView(R.layout.report_camera_activity_layout);
        ButterKnife.bind(this);
        super.initToolBarWithBack();


        sendBtn.setOnClickListener(sendButton -> {
            String email   = emailEt.getText().toString();
            String message = messageEt.getText().toString();
            if (!validate(email)){
                Toast.makeText(ReportCameraActivity.this, R.string.report_camera_wrong_email, Toast.LENGTH_SHORT).show();
                return;
            }
            if(message.isEmpty()){
                Toast.makeText(ReportCameraActivity.this, R.string.report_camera_toast_empty_message, Toast.LENGTH_SHORT).show();
                return;
            }
            emailEt.setEnabled(false);
            messageEt.setEnabled(false);
            sendBtn.setEnabled(false);
            firebaseTracker.logSendingMessage(email, message);
            presenter.sendMessage(email, message);
        });
        presenter.bindView(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.unBindView();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void messageSent() {
        Toast.makeText(this.getApplicationContext(), R.string.report_camera_toast_message_sent, Toast.LENGTH_LONG).show();
    }

    @Override
    public void messageFailed() {
        Toast.makeText(this.getApplicationContext(), R.string.report_camera_toast_message_not_sent, Toast.LENGTH_LONG).show();
        emailEt.setEnabled(true);
        messageEt.setEnabled(true);
        sendBtn.setEnabled(true);
    }

    private static boolean validate(String emailStr) {
        Matcher matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(emailStr);
        return matcher.find();
    }
}
