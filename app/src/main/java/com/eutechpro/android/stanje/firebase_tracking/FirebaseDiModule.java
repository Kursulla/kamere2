package com.eutechpro.android.stanje.firebase_tracking;


import android.content.Context;

import dagger.Module;
import dagger.Provides;

@Module
public class FirebaseDiModule {
    private Context context;

    public FirebaseDiModule(Context context) {
        this.context = context;
    }

    @Provides
    FirebaseTracker providesFirebaseTracker() {
        return new FirebaseTracker(context);
    }
}
