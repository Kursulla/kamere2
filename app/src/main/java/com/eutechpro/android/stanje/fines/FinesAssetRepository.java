package com.eutechpro.android.stanje.fines;

import com.eutechpro.android.stanje.commons.AssetsReader;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.List;
import io.reactivex.Observable;

/**
 * Fines repository that uses Assets for data store.
 */
class FinesAssetRepository implements FinesRepository {
    private String       dataSourceFile;
    private AssetsReader assetsReader;

    FinesAssetRepository(AssetsReader assetsReader, String dataSourceFile) {
        this.assetsReader = assetsReader;
        this.dataSourceFile = dataSourceFile;
    }

    @Override
    public Observable<List<Fine>> getAllFines() {
        return Observable.create(subscriber -> {
            String data = assetsReader.readAssetFrom(dataSourceFile);
            Gson   gson = new Gson();
            List<Fine> fines = gson.fromJson(data, new TypeToken<List<Fine>>() {
            }.getType());
            subscriber.onNext(fines);
            subscriber.onComplete();
        });
    }
}
