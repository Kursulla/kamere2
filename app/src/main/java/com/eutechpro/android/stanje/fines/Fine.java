package com.eutechpro.android.stanje.fines;

import com.google.gson.annotations.SerializedName;

/**
 * Entity for holding information about traffic fine.
 */
@SuppressWarnings("WeakerAccess")
class Fine {
    /** Brief description of violation */
    private String description;
    /** Number of points driver will be penalised with, if no further damage has been made */
    @SerializedName("light_points")
    private int lightPoints;
    /** Amount of money fine driver will have to pay, if no further damage has been made */
    @SerializedName("light_fine")
    private String lightFine;
    /** Driving licence restriction that might be brought upon driver, if no further damage has been made */
    @SerializedName("light_restriction")
    private String lightRestriction;
    /** Number of points driver will be penalised with, if further damage has been made (accident, injured...)*/
    @SerializedName("hard_points")
    private int hardPoints;
    /** Amount of money fine driver will have to pay, if further damage has been made (accident, injured...) */
    @SerializedName("hard_fine")
    private String hardFine;
    /** Driving licence restriction that might be brought upon driver, if further damage has been made (accident, injured...) */
    @SerializedName("hard_restriction")
    private String hardRestriction;


    public String getDescription() {
        return description;
    }

    public int getLightPoints() {
        return lightPoints;
    }

    public String getLightFine() {
        return lightFine;
    }

    public String getLightRestriction() {
        return lightRestriction;
    }

    public int getHardPoints() {
        return hardPoints;
    }

    public String getHardFine() {
        return hardFine;
    }

    public String getHardRestriction() {
        return hardRestriction;
    }
}
