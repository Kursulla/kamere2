package com.eutechpro.android.stanje.border;

import android.util.Log;

import com.eutechpro.android.stanje.R;
import com.eutechpro.android.stanje.data.entities.MapPoint;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;


class MvpPresenter implements Mvp.Presenter {
    private static final String TAG = "BorderCrossingPresenter";
    private Mvp.View            view;
    private Mvp.Model           model;
    private CompositeDisposable subscriptions;

    MvpPresenter(Mvp.Model model) {
        this.model = model;
        subscriptions = new CompositeDisposable();
    }

    @Override
    public void bindView(Mvp.View viewToBind) {
        this.view = viewToBind;
        Disposable disposable = model.getBorderCrossing()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(borderCrossings -> {
                    if (view == null){
                        return;
                    }
                    view.drawMarkersOnMap(borderCrossings);
                }, throwable -> {
                    Log.e(TAG, "Unable to get border crossings from DB:-> " + throwable.getLocalizedMessage());
                    view.showError(R.string.borders_error_unable_to_load_data);
                });
        subscriptions.add(disposable);
    }

    @Override
    public void unBindView() {
        this.view = null;
        subscriptions.clear();
    }

    @Override
    public void tapOnMapMarker(MapPoint mapPoint) {
        view.showRoadworkDetails(mapPoint);
    }
}
