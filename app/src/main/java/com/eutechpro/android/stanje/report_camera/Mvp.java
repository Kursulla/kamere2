package com.eutechpro.android.stanje.report_camera;

import com.eutechpro.android.stanje.BaseMvpPresenter;

import io.reactivex.Observable;

interface Mvp {
    interface Model {
        Observable<Boolean> reportCamera(String email, String message);
    }

    interface View {
        void messageSent();

        void messageFailed();
    }

    interface Presenter extends BaseMvpPresenter<View>{
        void sendMessage(String email, String message);
    }
}
