package com.eutechpro.android.stanje.commons;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v4.widget.NestedScrollView;
import android.util.AttributeSet;
import android.widget.TextView;

import com.eutechpro.android.stanje.R;

import butterknife.BindView;
import butterknife.ButterKnife;


public class DetailsBottomSheet extends NestedScrollView {
    @BindView(R.id.bottom_sheet_title)
    TextView title;
    @BindView(R.id.bottom_sheet_description)
    TextView description;
    private BottomSheetBehavior behaviour;
    private boolean expanded;

    public DetailsBottomSheet(@NonNull Context context) {
        super(context);
        inflate(getContext(), R.layout.bottom_sheet_details, this);
        ButterKnife.bind(this);
    }

    public DetailsBottomSheet(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        inflate(getContext(), R.layout.bottom_sheet_details, this);
        ButterKnife.bind(this);
    }

    public DetailsBottomSheet(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        inflate(getContext(), R.layout.bottom_sheet_details, this);
        ButterKnife.bind(this);
    }

    public void init() {
        behaviour = BottomSheetBehavior.from(this);
        behaviour.setPeekHeight(0);
        behaviour.setHideable(true);
    }

    public void setTitle(String title) {
        this.title.setText(title);
    }

    public void setDescription(String description) {
        this.description.setText(description);
    }

    public void collapse() {
        behaviour.setState(BottomSheetBehavior.STATE_COLLAPSED);
    }

    public void expand() {
        behaviour.setState(BottomSheetBehavior.STATE_EXPANDED);
    }

    public boolean isExpanded() {
        return behaviour.getState() == BottomSheetBehavior.STATE_EXPANDED;
    }
}
