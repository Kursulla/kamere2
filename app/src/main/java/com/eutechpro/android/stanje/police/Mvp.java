package com.eutechpro.android.stanje.police;

import io.reactivex.Observable;

interface Mvp {
    interface Model {
        Observable<Boolean> getStream();
        void subscribe(String email, String name);
    }

    interface View {
        void sendingSuccessful();

        void sendingUnSuccessful();
    }

    interface Presenter {
        void bindView(View view);

        void unBind();

        void subscribe(String email, String name);
    }
}
