package com.eutechpro.android.stanje.fines;

import com.eutechpro.android.stanje.ApplicationDiComponent;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(
        modules = {
                FinesPresenterDiModule.class
        },
        dependencies = ApplicationDiComponent.class
)
public interface FinesDiComponent {
    void inject(FinesActivity activity);
}
