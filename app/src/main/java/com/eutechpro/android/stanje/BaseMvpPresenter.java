package com.eutechpro.android.stanje;

public interface BaseMvpPresenter<T> {
    void bindView(T view);

    void unBindView();
}
