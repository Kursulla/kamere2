package com.eutechpro.android.stanje.splash;

import com.eutechpro.android.stanje.ApplicationDiComponent;

import dagger.Component;

@Component(
        modules = SplashScreenPresenterDiModule.class,
        dependencies = ApplicationDiComponent.class
)
public interface SplashScreenDiComponent {
    void inject(SplashScreenActivity splashScreenActivity);
}
