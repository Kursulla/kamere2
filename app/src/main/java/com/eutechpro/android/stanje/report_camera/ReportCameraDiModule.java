package com.eutechpro.android.stanje.report_camera;

import com.eutechpro.android.stanje.data.rest.RetrofitFactory;

import dagger.Module;
import dagger.Provides;

@Module
public class ReportCameraDiModule {
    @Provides
    public Mvp.Presenter providesPresenter() {
        UserCommunicationEndpoints apiEndpoints = RetrofitFactory
                .generateRetrofit()
                .create(UserCommunicationEndpoints.class);
        return new MvpPresenter(new MvpModel(new UserCommunicationService(apiEndpoints)));
    }
}
