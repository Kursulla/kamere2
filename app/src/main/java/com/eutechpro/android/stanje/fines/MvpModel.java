package com.eutechpro.android.stanje.fines;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;


class MvpModel implements Mvp.Model {
    private static final String     TAG      = "MvpModel:Mvp";
    private              List<Fine> allFines = new ArrayList<>();

    MvpModel(FinesRepository repository) {
        repository.getAllFines()
                .subscribe(
                        fines -> allFines = fines,
                        throwable -> Log.e(TAG, "Exception fetching fines: " + throwable.getLocalizedMessage())
                );
    }

    @Override
    public Observable<List<Fine>> getFinesByQuery(String query) {
        return Observable.just(allFines)
                .debounce(400, TimeUnit.MILLISECONDS)
                .map(fines -> {
                            List<Fine> filteredList = new ArrayList<>();
                            for (Fine fine : fines) {
                                if (fine.getDescription().contains(query)){
                                    filteredList.add(fine);
                                }
                            }
                            return filteredList;
                        }
                );
    }
}
