package com.eutechpro.android.stanje.report_camera;

import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;

/**
 * Retrofit implementation of {@link UserCommunicationEndpoints}
 */
class UserCommunicationService {

    private UserCommunicationEndpoints apiEndpoints;

    UserCommunicationService(UserCommunicationEndpoints apiEndpoints) {
        this.apiEndpoints = apiEndpoints;
    }

    Observable<Boolean> reportCamera(String email, final String message) {
        return apiEndpoints.reportCamera(email, message)
                .subscribeOn(Schedulers.io())
                .flatMap(messageResponse -> {
                    if (messageResponse.getMessage() != null && messageResponse.getMessage().contains("success")){
                        return Observable.just(true);
                    } else {
                        return Observable.just(false);
                    }
                });
    }
}
