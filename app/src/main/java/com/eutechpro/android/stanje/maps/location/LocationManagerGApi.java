package com.eutechpro.android.stanje.maps.location;

import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

public class LocationManagerGApi implements SimpleLocationManager {
    private static final String TAG = "LocationManager";
    private static final int SMALLEST_DISPLACEMENT_METERS = 5;
    private static final int FASTEST_INTERVAL = 3000;
    private static final int REFRESH_INTERVAL = 3000;

    private final GoogleApiClient         googleApiClient;
    private       LocationChangesListener locationChangesListener;
    private       LocationManagerListener locationManagerListener;

    public LocationManagerGApi(Context context) {
        googleApiClient = new GoogleApiClient.Builder(context)
                .addConnectionCallbacks(new ConnectionCallbacks() {
                    @Override
                    public void onConnected(@Nullable Bundle bundle) {
                        Log.d(TAG, "onConnected");
                        startLocationUpdates();
                        if (locationManagerListener != null) {
                            locationManagerListener.connected();
                        }
                    }

                    @Override
                    public void onConnectionSuspended(int i) {
                        Log.d(TAG, "onConnectionSuspended: " + i);
                        if (locationManagerListener != null) {
                            locationManagerListener.suspended();
                        }
                    }
                })
                .addOnConnectionFailedListener(new OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
                        Log.d(TAG, "onConnectionFailed: " + connectionResult.getErrorMessage());
                        if (locationManagerListener != null) {
                            locationManagerListener.failed();
                        }
                    }
                })
                .addApi(LocationServices.API)
                .build();
    }

    @Override
    public void connect() {
        Log.d(TAG, "connect");
        googleApiClient.connect();
    }

    @Override
    public void disconnect() {
        Log.d(TAG, "disconnect");
        googleApiClient.disconnect();
    }

    @Override
    public void stopLocationUpdates() {
        if (googleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(googleApiClient, new LocationListener() {
                @Override
                public void onLocationChanged(Location location) {
                    Log.d(TAG, "onLocationChanged in stop");
                }
            });
        }
    }

    @Override
    public void startLocationUpdates() {
        if (googleApiClient.isConnected()) {
            LocationRequest locationRequest = new LocationRequest();
            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            locationRequest.setInterval(REFRESH_INTERVAL);
            locationRequest.setFastestInterval(FASTEST_INTERVAL);
            locationRequest.setSmallestDisplacement(SMALLEST_DISPLACEMENT_METERS);
            LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, new LocationListener() {
                @Override
                public void onLocationChanged(Location location) {
                    Log.d(TAG, "onLocationChanged");
                    if (location != null) {
                        Log.d(TAG, "Location is valid: " + location.getLatitude() + " " + location.getLongitude());
                        if (locationChangesListener != null) {
                            locationChangesListener.locationChanged(location);
                        }
                        if (locationManagerListener != null) {
                            locationManagerListener.locationChanged(location);
                        }
                    }
                }
            });
            Log.w(TAG, "Started location updates.");
        } else {
            Log.w(TAG, "You are trying to start location updates, and GoogleApiClient is not connected at all!!!");
        }
    }

    @Override
    @SuppressWarnings("unused")
    public void setLocationChangesListener(LocationChangesListener locationChangesListener) {
        this.locationChangesListener = locationChangesListener;
    }

    @Override
    @SuppressWarnings("unused")
    public void setLocationManagerListener(LocationManagerListener locationManagerListener) {
        this.locationManagerListener = locationManagerListener;
    }
}
