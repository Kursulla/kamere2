package com.eutechpro.android.stanje.splash;

import android.util.Log;

import com.eutechpro.android.stanje.cameras.CamerasRepository;
import com.eutechpro.android.stanje.cameras.CamerasRestApi;
import com.eutechpro.android.stanje.commons.RefreshTracker;
import com.eutechpro.android.stanje.data.entities.MapPoint;
import com.eutechpro.android.stanje.roadworks.RoadworksRepository;
import com.eutechpro.android.stanje.roadworks.RoadworksRestApi;

import java.util.Collections;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.functions.Function;
import io.reactivex.subjects.PublishSubject;

@SuppressWarnings("Convert2MethodRef")
public class MvpModel implements Mvp.Model {
    private static final String TAG = "Splash MvpModel";
    private CamerasRestApi                 camerasRestApi;
    private CamerasRepository              camerasRepository;
    private RoadworksRepository            roadworksRepository;
    private RoadworksRestApi               roadworksRestApi;
    private PublishSubject<FetchingResult> subject;
    private RefreshTracker                 refreshTracker;

    public MvpModel(
            CamerasRestApi camerasRestApi,
            CamerasRepository camerasRepository,
            RoadworksRestApi roadworksRestApi,
            RoadworksRepository roadworksRepository,
            RefreshTracker refreshTracker
    ) {
        this.camerasRepository = camerasRepository;
        this.roadworksRepository = roadworksRepository;
        this.camerasRestApi = camerasRestApi;
        this.roadworksRestApi = roadworksRestApi;
        this.subject = PublishSubject.create();
        this.refreshTracker = refreshTracker;
    }


    @Override
    public Observable<FetchingResult> dataStream() {
        return subject;
    }

    @Override
    public void refreshData() {
        Observable<List<MapPoint>> observableOnCameras = camerasRestApi
                .downloadCameras()
                .flatMap(new ReplaceCamerasInDb());


        Observable<List<MapPoint>> observableOnRoadworks = roadworksRestApi
                .downloadAllRoadworks()
                .flatMap(new ReplaceRoadworksInDb());

        Observable.zip(
                observableOnCameras,
                observableOnRoadworks,
                (cameras, roadworks) -> new FetchingResult(cameras, roadworks)
        ).subscribe(
                fetchingResult -> {
                    if (fetchingResult.containsAllData()){
                        refreshTracker.saveRefreshTimeStamp();
                    }
                    subject.onNext(fetchingResult);
                },
                throwable -> {
                    Log.e(TAG, "refreshData: ", throwable);
                    subject.onNext(new FetchingResult(throwable));
                }
        );
    }

    @Override
    public boolean isTimeForRefresh() {
        return refreshTracker.isTimeForRefresh();
    }

    private class ReplaceCamerasInDb implements Function<List<MapPoint>, ObservableSource<List<MapPoint>>> {
        @Override
        public ObservableSource<List<MapPoint>> apply(List<MapPoint> cameras) throws Exception {
            return camerasRepository.replaceAllCameras(cameras)
                    .flatMap(replaced -> Observable.just(cameras));
        }
    }

    private class ReplaceRoadworksInDb implements Function<List<MapPoint>, ObservableSource<List<MapPoint>>> {
        @Override
        public ObservableSource<List<MapPoint>> apply(List<MapPoint> roadworks) throws Exception {
            return roadworksRepository.replaceAllRoadworks(roadworks)
                    .flatMap(replaced -> Observable.just(roadworks));
        }
    }


    @SuppressWarnings("unused")
    static class FetchingResult {
        List<MapPoint> roadWorks;
        Throwable      exception;
        List<MapPoint> cameras;

        public FetchingResult(Throwable exception) {
            this.exception = exception;
            this.roadWorks = Collections.emptyList();
            this.cameras = Collections.emptyList();

        }

        FetchingResult(List<MapPoint> roadWorks, List<MapPoint> cameras) {
            this.roadWorks = roadWorks;
            this.cameras = cameras;
        }

        public void setException(Throwable exception) {
            this.exception = exception;
        }

        public Throwable getException() {
            return exception;
        }

        boolean containsAllData() {
            return !cameras.isEmpty() && !roadWorks.isEmpty();
        }
    }
}
