package com.eutechpro.android.stanje.border;

import com.eutechpro.android.stanje.data.entities.MapPoint;

import java.util.List;

import io.reactivex.Observable;

interface BordersRepository {
    Observable<List<MapPoint>> getBorderCrossings();
}
