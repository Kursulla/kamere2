package com.eutechpro.android.stanje.commons.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;

import com.eutechpro.android.stanje.data.entities.MapPoint;


public class DbHelper extends SQLiteOpenHelper {
    private static final String DATABASE_NAME    = "kamere2.db";
    private static final int    DATABASE_VERSION = 1;

    @Deprecated
    public static final String dbSyncReadingLock = "DB_SYNC_READING_LOCK";
    @Deprecated
    public static final String dbSyncWritingLock = "DB_SYNC_WRITING_LOCK";

    public static final String CAMERA_TABLE_NAME  = "cameras";
    public static final String CAMERA_TITLE       = "title";
    public static final String CAMERA_DESCRIPTION = "description";
    public static final String CAMERA_META        = "meta";
    public static final String CAMERA_TYPE        = "type";
    public static final String CAMERA_IMAGE       = "image";
    public static final String CAMERA_LAT         = "lat";
    public static final String CAMERA_LNG         = "lng";

    public static final String ROADWORKS_TABLE_NAME  = "roadworks";
    public static final String ROADWORKS_ID          = "id";
    public static final String ROADWORKS_TITLE       = "title";
    public static final String ROADWORKS_DESCRIPTION = "description";
    public static final String ROADWORKS_META        = "meta";
    public static final String ROADWORKS_TYPE        = "type";
    public static final String ROADWORKS_LAT         = "lat";
    public static final String ROADWORKS_LNG         = "lng";

    private static DbHelper dbHelperInstance;

    @VisibleForTesting
    public DbHelper(Context context, String dbName, int dbVersion) {
        super(context, dbName, null, dbVersion);
    }

    public static DbHelper getDbHelperInstance(Context context) {
        if(dbHelperInstance == null) {
            dbHelperInstance = new DbHelper(context, DATABASE_NAME, DATABASE_VERSION);
        }
        return dbHelperInstance;
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table " +
                           CAMERA_TABLE_NAME
                           + "(" +
                           CAMERA_TITLE + " text not null," +
                           CAMERA_DESCRIPTION + " text," +
                           CAMERA_META + " text," +
                           CAMERA_TYPE + " text," +
                           CAMERA_IMAGE + " text," +
                           CAMERA_LAT + " text not null," +
                           CAMERA_LNG + " text not null" +
                           ");");
        db.execSQL("create table " +
                           ROADWORKS_TABLE_NAME
                           + "(" +
                           ROADWORKS_ID + " text not null," +
                           ROADWORKS_TITLE + " text not null," +
                           ROADWORKS_DESCRIPTION + " text," +
                           ROADWORKS_META + " text," +
                           ROADWORKS_TYPE + " text," +
                           ROADWORKS_LAT + " text not null," +
                           ROADWORKS_LNG + " text not null" +
                           ");");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }


    @NonNull
    public static ContentValues roadworksModelToContentValues(MapPoint modelForSaving) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(DbHelper.ROADWORKS_ID, modelForSaving.getId());
        contentValues.put(DbHelper.ROADWORKS_TITLE, modelForSaving.getTitle());
        contentValues.put(DbHelper.ROADWORKS_DESCRIPTION, modelForSaving.getDescription());
        contentValues.put(DbHelper.ROADWORKS_META, modelForSaving.getMeta());
        contentValues.put(DbHelper.ROADWORKS_TYPE, modelForSaving.getType());
        contentValues.put(DbHelper.ROADWORKS_LAT, modelForSaving.getLat());
        contentValues.put(DbHelper.ROADWORKS_LNG, modelForSaving.getLng());
        return contentValues;
    }

    @NonNull
    public static MapPoint cursorToRoadworksModel(Cursor cursor) {
        MapPoint model = new MapPoint();
        model.setId(cursor.getLong(cursor.getColumnIndex(DbHelper.ROADWORKS_ID)));
        model.setTitle(cursor.getString(cursor.getColumnIndex(DbHelper.ROADWORKS_TITLE)));
        model.setDescription(cursor.getString(cursor.getColumnIndex(DbHelper.ROADWORKS_DESCRIPTION)));
        model.setMeta(cursor.getString(cursor.getColumnIndex(DbHelper.ROADWORKS_META)));
        model.setType(cursor.getString(cursor.getColumnIndex(DbHelper.ROADWORKS_TYPE)));
        model.setLat(cursor.getString(cursor.getColumnIndex(DbHelper.ROADWORKS_LAT)));
        model.setLng(cursor.getString(cursor.getColumnIndex(DbHelper.ROADWORKS_LNG)));
        return model;
    }

    @NonNull
    public static ContentValues cameraModelToContentValues(MapPoint modelForSaving) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(DbHelper.CAMERA_TITLE, modelForSaving.getTitle());
        contentValues.put(DbHelper.CAMERA_DESCRIPTION, modelForSaving.getDescription());
        contentValues.put(DbHelper.CAMERA_META, modelForSaving.getMeta());
        contentValues.put(DbHelper.CAMERA_IMAGE, modelForSaving.getImage());
        contentValues.put(DbHelper.CAMERA_TYPE, modelForSaving.getType());
        contentValues.put(DbHelper.CAMERA_LAT, modelForSaving.getLat());
        contentValues.put(DbHelper.CAMERA_LNG, modelForSaving.getLng());
        return contentValues;
    }

    @NonNull
    public static MapPoint cursorToCameraModel(Cursor cursor) {
        MapPoint model = new MapPoint();
        model.setTitle(cursor.getString(cursor.getColumnIndex(DbHelper.CAMERA_TITLE)));
        model.setDescription(cursor.getString(cursor.getColumnIndex(DbHelper.CAMERA_DESCRIPTION)));
        model.setMeta(cursor.getString(cursor.getColumnIndex(DbHelper.CAMERA_META)));
        model.setType(cursor.getString(cursor.getColumnIndex(DbHelper.CAMERA_TYPE)));
        model.setImage(cursor.getString(cursor.getColumnIndex(DbHelper.CAMERA_IMAGE)));
        model.setLat(cursor.getString(cursor.getColumnIndex(DbHelper.CAMERA_LAT)));
        model.setLng(cursor.getString(cursor.getColumnIndex(DbHelper.CAMERA_LNG)));

        return model;
    }

}
