package com.eutechpro.android.stanje.fines;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.eutechpro.android.stanje.BaseActivity;
import com.eutechpro.android.stanje.KamereApplication;
import com.eutechpro.android.stanje.R;
import com.eutechpro.android.stanje.firebase_tracking.FirebaseTracker;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;


public class FinesActivity extends BaseActivity implements Mvp.View {
    private static final String TAG = "FinesActivity";
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.progress_bar)
    ProgressBar progressBar;
    @BindView(R.id.fine_title)
    TextView bottomTitle;
    @BindView(R.id.light_points)
    TextView bottomLightPoints;
    @BindView(R.id.hard_points)
    TextView bottomHardPoints;
    @BindView(R.id.light_money)
    TextView bottomLightMoney;
    @BindView(R.id.hard_money)
    TextView bottomHardMoney;
    @BindView(R.id.light_restrictions)
    TextView bottomLightRestrictions;
    @BindView(R.id.hard_restrictions)
    TextView bottomHardRestrictions;

    @Inject
    FirebaseTracker firebaseTracker;
    @Inject
    Mvp.Presenter   presenter;

    private FinesAdapter        adapter;
    private BottomSheetBehavior bottomSheetBehavior;
    private Typeface            font;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Inject
        KamereApplication.get(this).getFinesComponent().inject(this);
        //Set layout
        setContentView(R.layout.fines_activity_layout);
        ButterKnife.bind(this);
        super.initToolBarWithBack();

        font = Typeface.createFromAsset(getAssets(), "fonts/roboto_light.ttf");

        recyclerView = setupRecyclerView();

        NestedScrollView bottomSheet = findViewById(R.id.bottom_sheet);
        if(bottomSheet != null) {
            bottomSheetBehavior = BottomSheetBehavior.from(bottomSheet);
            bottomSheetBehavior.setPeekHeight(0);
            bottomSheetBehavior.setHideable(true);
        }
        presenter.bindView(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.unBindView();
    }

    @Override
    public void onBackPressed() {
        if (isFineDetailsViewShown()){
            hideFineDetailsView();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.fines_menu, menu);
        final MenuItem   allMenuItems = menu.findItem(R.id.action_search);
        final SearchView searchView   = (SearchView) allMenuItems.getActionView();
        searchView.findViewById(android.support.v7.appcompat.R.id.search_src_text)
                .setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View view, MotionEvent motionEvent) {
                        if (isFineDetailsViewShown()){
                            hideFineDetailsView();
                        }
                        view.performClick();
                        return false;
                    }
                });
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                allMenuItems.collapseActionView();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String searchString) {
                Log.d(TAG, "onQueryTextChange: " + searchString);
                firebaseTracker.logFinesFilteringQuery(searchString);
                presenter.filterFines(searchString);
                progressBar.setVisibility(View.VISIBLE);
                if (isFineDetailsViewShown()){
                    hideFineDetailsView();
                }
                return false;
            }
        });
        searchView.setOnQueryTextFocusChangeListener((view, focused) -> {
            if (focused){
                hideFineDetailsView();
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    private RecyclerView setupRecyclerView() {
        if (recyclerView != null) {
            recyclerView.setHasFixedSize(true);
            LinearLayoutManager layoutManager = new LinearLayoutManager(this);
            recyclerView.setLayoutManager(layoutManager);
        }
        return recyclerView;
    }

    @Override
    public void showInitData(List<Fine> allFines) {
        if (adapter == null) {
            adapter = new FinesAdapter(allFines, font);
            adapter.setOnItemTapListener(fine -> showFineDetailsView(fine));
            recyclerView.setAdapter(adapter);
        } else {
            adapter.replaceData(allFines);
        }
        adapter.notifyDataSetChanged();
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showFilteredData(List<Fine> filteredFines) {
        adapter.replaceData(filteredFines);
        adapter.notifyDataSetChanged();
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showFineDetailsView(Fine fine) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null){
            imm.hideSoftInputFromWindow(getWindow().getDecorView().getWindowToken(), 0);
        }

        if (bottomTitle != null) {
            bottomTitle.setTypeface(font);
            bottomTitle.setText(fine.getDescription());
        }
        if (bottomLightPoints != null) {
            bottomLightPoints.setTypeface(font);
            bottomLightPoints.setText(String.valueOf(fine.getLightPoints()));
        }
        if (bottomHardPoints != null) {
            bottomHardPoints.setTypeface(font);
            bottomHardPoints.setText(String.valueOf(fine.getHardPoints()));
        }
        if (bottomLightMoney != null) {
            bottomLightMoney.setTypeface(font);
            bottomLightMoney.setText(String.valueOf(fine.getLightFine()));
        }
        if (bottomHardMoney != null) {
            bottomHardMoney.setTypeface(font);
            bottomHardMoney.setText(String.valueOf(fine.getHardFine()));
        }
        if (bottomLightRestrictions != null) {
            bottomLightRestrictions.setTypeface(font);
            bottomLightRestrictions.setText(String.valueOf(fine.getLightRestriction()));
        }
        if (bottomHardRestrictions != null) {
            bottomHardRestrictions.setTypeface(font);
            bottomHardRestrictions.setText(String.valueOf(fine.getHardRestriction()));
        }
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);

        firebaseTracker.logFineDetailsOpening(fine.getDescription());
    }

    @Override
    public void hideFineDetailsView() {
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
    }

    @Override
    public boolean isFineDetailsViewShown() {
        return bottomSheetBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED;
    }

    @Override
    public void showError(int message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    private static class FinesAdapter extends RecyclerView.Adapter<FinesAdapter.FinesViewHolder> {
        private List<Fine> fines;
        private OnItemTapListener onItemTapListener;
        private Typeface font;

        FinesAdapter(List<Fine> fines, Typeface font) {
            this.fines = fines;
            this.font = font;
        }

        void setOnItemTapListener(OnItemTapListener onItemTapListener) {
            this.onItemTapListener = onItemTapListener;
        }

        @Override
        public FinesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.fines_list_item, parent, false);
            return new FinesViewHolder(v);
        }

        @Override
        public void onBindViewHolder(FinesViewHolder holder, final int position) {
            holder.title.setTypeface(font);
            holder.title.setText(fines.get(holder.getAdapterPosition()).getDescription());
            if(onItemTapListener != null){
                holder.cell.setOnClickListener(view -> onItemTapListener.tapOn(fines.get(holder.getAdapterPosition())));
            }
        }

        @Override
        public int getItemCount() {
            return fines.size();
        }

        void replaceData(List<Fine> allFines) {
            fines.clear();
            fines.addAll(allFines);
        }

        /**
         * Listener for notifying adapter consumer that user tapped on item.
         */
        interface OnItemTapListener {
            void tapOn(Fine fine);
        }

        static class FinesViewHolder extends RecyclerView.ViewHolder {
            TextView  title;
            ViewGroup cell;
            private FinesViewHolder(View v) {
                super(v);
                title = v.findViewById(R.id.title);
                cell = v.findViewById(R.id.cell);
            }
        }
    }
}
