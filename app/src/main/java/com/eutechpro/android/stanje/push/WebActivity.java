package com.eutechpro.android.stanje.push;

import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class WebActivity extends Activity {

    public static final String EXTRAS_KEY_URL = "EXTRAS_KEY_URL";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getIntent().getExtras();
        if(bundle == null) {
            finish();
            return;
        }
        String url = bundle.getString(EXTRAS_KEY_URL);
        if(url == null || url.isEmpty()) {
            finish();
            return;
        }
        WebView webView = new WebView(this);
        webView.setWebViewClient(new WebViewClient());
        webView.loadUrl(url);
        setContentView(webView);
    }
}
