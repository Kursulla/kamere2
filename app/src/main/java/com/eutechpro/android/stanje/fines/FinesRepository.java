package com.eutechpro.android.stanje.fines;

import java.util.List;

import io.reactivex.Observable;

interface FinesRepository {
    Observable<List<Fine>> getAllFines();
}
