package com.eutechpro.android.stanje.sync;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.ContentResolver;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;

/**
 * SyncAdapter for refreshing:
 * <ul>
 * <li>Cameras</li>
 * </ul>
 */
public class SyncAdapterManager {
    private static final String TAG          = "SyncAdapterManager";
    private static final String AUTHORITY    = "com.eutechpro.android.stanje.provider";
    private static final String ACCOUNT_TYPE = "com.eutechpro.android.stanje";
    private static final String ACCOUNT      = "Stanje na putevima";
    private static Account newAccount;
    private static final int HOUR          = 60 * 60;
    private static final int SYNC_INTERVAL = 6 * HOUR;

    private SyncAdapterManager() {
    }

    /**
     * Init SyncAdapter to start its journey.
     *
     * @param context context
     */
    public static void init(Context context) {
        newAccount = new Account(ACCOUNT, ACCOUNT_TYPE);
        AccountManager accountManager = (AccountManager) context.getSystemService(Context.ACCOUNT_SERVICE);
         /*
          * Add the account and account type, no password or user data
          * If successful, return the Account object, otherwise report an error.
          */
        if (accountManager == null){
            Log.e(TAG, "init: accountManager is null! Can't procede with SyncManager init");
            return;
        }
        if(!accountManager.addAccountExplicitly(newAccount, "", null)) {
            Log.d(TAG, "addAccountExplicitly not successful");
        }

        ContentResolver.addPeriodicSync(newAccount, AUTHORITY, new Bundle(), SYNC_INTERVAL);
        ContentResolver.setSyncAutomatically(newAccount, AUTHORITY, true);
    }

    /**
     * In special cases, we might want to force refreshing immediately, and not to wait for regular period of time
     */
    @SuppressWarnings("unused")
    public static void forceRefresh() {
        Bundle bundle = new Bundle();
        bundle.putBoolean(ContentResolver.SYNC_EXTRAS_EXPEDITED, true);
        bundle.putBoolean(ContentResolver.SYNC_EXTRAS_MANUAL, true);

        ContentResolver.requestSync(newAccount, AUTHORITY, bundle);
    }
}
