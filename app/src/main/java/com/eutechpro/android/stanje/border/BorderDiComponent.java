package com.eutechpro.android.stanje.border;

import com.eutechpro.android.stanje.ApplicationDiComponent;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(
        modules = {
                BorderPresenterDiModule.class
        },
        dependencies = ApplicationDiComponent.class
)
public interface BorderDiComponent {
    void inject(BorderActivity activity);
}
