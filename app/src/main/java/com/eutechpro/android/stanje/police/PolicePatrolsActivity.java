package com.eutechpro.android.stanje.police;

import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.eutechpro.android.stanje.BaseActivity;
import com.eutechpro.android.stanje.R;
import com.eutechpro.android.stanje.firebase_tracking.FirebaseTracker;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PolicePatrolsActivity extends BaseActivity implements Mvp.View {
    private static final Pattern VALID_EMAIL_ADDRESS_REGEX = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);

    @BindView(R.id.email)
    EditText emailEt;
    @BindView(R.id.name)
    EditText nameEt;
    @BindView(R.id.send_btn)
    Button   sendBtn;

    private Mvp.Presenter presenter;
    @Inject
    FirebaseTracker firebaseTracker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        presenter = new Presenter(new Model());

        //Set layout
        setContentView(R.layout.police_patrol_activity_layout);
        ButterKnife.bind(this);
        super.initToolBarWithBack();


        sendBtn.setOnClickListener(sendButton -> {
            String email   = emailEt.getText().toString();
            String name = nameEt.getText().toString();
            if (!validate(email)){
                Toast.makeText(PolicePatrolsActivity.this, R.string.police_patrols_wrong_email, Toast.LENGTH_SHORT).show();
                return;
            }
            if(name.isEmpty()){
                Toast.makeText(PolicePatrolsActivity.this, R.string.police_patrols_toast_empty_name, Toast.LENGTH_SHORT).show();
                return;
            }
            emailEt.setEnabled(false);
            nameEt.setEnabled(false);
            sendBtn.setEnabled(false);
//            firebaseTracker.logSendingMessage(email, name);
            presenter.subscribe(email, name);
        });
        presenter.bindView(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.unBind();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void sendingSuccessful() {
        Toast.makeText(this.getApplicationContext(), "Uspesno ste se prijavili", Toast.LENGTH_LONG).show();
    }

    @Override
    public void sendingUnSuccessful() {
        Toast.makeText(this.getApplicationContext(), "Doslo je do problema. Pokusajte ponovo", Toast.LENGTH_LONG).show();
        emailEt.setEnabled(true);
        nameEt.setEnabled(true);
        sendBtn.setEnabled(true);
    }

    private static boolean validate(String emailStr) {
        Matcher matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(emailStr);
        return matcher.find();
    }
}
