package com.eutechpro.android.stanje.maps;

import android.location.Location;
import android.util.Log;

import com.eutechpro.android.stanje.data.entities.MapPoint;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GoogleMapManager implements OnMapReadyCallback {
    private static final String TAG = "MapManager";
    private List<MapPoint>           mapPoints;
    private Map<String, Integer>     markerIds;
    private GoogleMap                map;
    private OnMapInteractionListener onMapInteractionListener;
    private int                      mapPointIcon;

    public GoogleMapManager(int mapPointIcon) {
        this.mapPointIcon = mapPointIcon;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.map = googleMap;
        map.setMyLocationEnabled(true);
        map.getUiSettings().setZoomControlsEnabled(true);
        if(mapPoints != null) {
            drawMarkersOnMap(mapPoints);
        }
        //Init location is center of Serbia with default zoom
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(44.1768866, 20.7058481), 7));
    }
    /**
     * Return list of all map points.
     *
     * @return List of map points.
     */
    public List<MapPoint> getMapPoints() {
        return mapPoints;
    }

    /**
     * Run async map preparation which ends in calling {@link OnMapReadyCallback#onMapReady(GoogleMap)} callback which we use to init {@link GoogleMap}.
     *
     * @param mapFragment {@link SupportMapFragment} we use to init {@link GoogleMap} in.
     */
    public void attachTo(SupportMapFragment mapFragment) {
        mapFragment.getMapAsync(this);
    }

    public void setOnMapInteractionListener(OnMapInteractionListener onMapInteractionListener) {
        this.onMapInteractionListener = onMapInteractionListener;
    }

    /**
     * Draw {@link MapPoint} markerIds on the {@link GoogleMap}. If {@link GoogleMap} is not ready, list of mapPoints will be saved, and upond successfull {@link GoogleMap} initialisation, markerIds will be drawn on the map.
     *  @param mapPoints {@link List} of {@link MapPoint} that has to be drawn on the map.
     *
     */
    public void drawMarkersOnMap(final List<MapPoint> mapPoints) {
        if (mapPoints == null) {
            throw new IllegalArgumentException("You provided null for list of mapPoints!!!");
        }
        if (map == null) {
            this.mapPoints = mapPoints;
            Log.d(TAG, "Map is not ready yet. We will wait map to become ready and draw mapPoints on it");
            return;
        }
        markerIds = new HashMap<>(mapPoints.size());
        for (int i = 0; i< mapPoints.size(); i++) {
            MapPoint mapPoint = mapPoints.get(i);
            LatLng markerLocation = new LatLng(mapPoint.getLatDouble(), mapPoint.getLngDouble());
            Marker marker = map.addMarker(new MarkerOptions()
                    .position(markerLocation)
                    .title(mapPoint.getTitle())
                    .icon(BitmapDescriptorFactory.fromResource(mapPointIcon)));
            markerIds.put(marker.getId(), i);
        }
        map.setOnMarkerClickListener(
                marker -> {
                    if (onMapInteractionListener != null){
                        int position = markerIds.get(marker.getId());

                        onMapInteractionListener.onMarkerClick(mapPoints.get(position));
                    }
                    return false;
        });
        this.mapPoints = null;
    }

    /**
     * Clear all markerIds from the {@link GoogleMap}. <br/> If {@link GoogleMap} is null, this method will have no effect.
     */
    public void clearMapMarkers() {
        if (map != null) {
            map.clear();
        }
    }

    /**
     * Move {@link GoogleMap} camera to the specified location and zoom to desired level.
     *
     * @param location  {@link Location} we have to focus our {@link GoogleMap}.
     * @param zoomLevel amount for zoom
     */
    public void moveToLocation(Location location, float zoomLevel) {
        map.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(), location.getLongitude()), zoomLevel));
    }

    /**
     * Move map camera to desired location.
     * @param location Location to move camera to.
     */
    public void moveToLocationAndKeepZoomLevel(Location location) {
        map.animateCamera(CameraUpdateFactory.newLatLng(new LatLng(location.getLatitude(), location.getLongitude())));
    }

    public void focusUserAndMarker(LatLng usersLocation, LatLng cameraLocation) {
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        builder.include(usersLocation);
        builder.include(cameraLocation);

        CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(builder.build(), 150);
        map.animateCamera(cu);
    }

    public interface OnMapInteractionListener{
        void onMarkerClick(MapPoint mapPoint);
    }
}
