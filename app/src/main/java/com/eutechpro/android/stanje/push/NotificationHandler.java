package com.eutechpro.android.stanje.push;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;

import com.eutechpro.android.stanje.R;
import com.eutechpro.android.stanje.cameras.CamerasActivity;

import static android.support.v4.app.NotificationCompat.PRIORITY_MAX;

class NotificationHandler {

    void fireWebNotification(String title, String description, String url, Context context) {
        Intent resultIntent = new Intent(context, WebActivity.class);
        resultIntent.putExtra(WebActivity.EXTRAS_KEY_URL, url);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
        stackBuilder.addParentStack(CamerasActivity.class);
        stackBuilder.addNextIntent(resultIntent);

        createNotificationEntry(title, description, context, stackBuilder);
    }

    void firePlayNotification(String title, String description, String packageName, Context context) {
        Intent resultIntent = new Intent(
                Intent.ACTION_VIEW,
                Uri.parse("market://details?id=" + packageName));
        resultIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
        stackBuilder.addParentStack(CamerasActivity.class);
        stackBuilder.addNextIntent(resultIntent);

        createNotificationEntry(title, description, context, stackBuilder);
    }


    private void createNotificationEntry(String title, String description, Context context, TaskStackBuilder stackBuilder) {
        NotificationCompat.Builder builder =
                new NotificationCompat.Builder(context,"info")
                        .setSmallIcon(R.drawable.notification_icon)
                        .setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.drawable.notification_icon))

                        .setContentTitle(title)
                        .setAutoCancel(true)
                        .setPriority(PRIORITY_MAX)
                        .setContentText(description)
                        .setDefaults(Notification.DEFAULT_LIGHTS | Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE)
                        .setStyle(new NotificationCompat.BigTextStyle().bigText(description));


        PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
        builder.setContentIntent(resultPendingIntent);

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        if (notificationManager != null){
            notificationManager.notify((int) System.currentTimeMillis(), builder.build());
        }
    }


}
