package com.eutechpro.android.stanje.cameras;

import com.eutechpro.android.stanje.ApplicationDiComponent;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(
        modules = {
                CamerasPresenterDiModule.class
        },
        dependencies = ApplicationDiComponent.class
)
public interface CamerasDiComponent {
    void inject(CamerasActivity activity);
}
