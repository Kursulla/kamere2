package com.eutechpro.android.stanje.roadworks;

import com.eutechpro.android.stanje.data.entities.MapPoint;

import java.util.List;

import io.reactivex.Observable;

class MvpModel implements Mvp.Model {
    private RoadworksRepository repository;

    MvpModel(RoadworksRepository repository) {
        this.repository = repository;
    }

    @Override
    public Observable<List<MapPoint>> getAllRoadWorksRx() {
        return repository.getRoadworksRx();
    }
}
