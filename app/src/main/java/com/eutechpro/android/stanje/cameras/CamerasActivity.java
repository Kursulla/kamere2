package com.eutechpro.android.stanje.cameras;

import android.location.Location;
import android.os.Bundle;
import android.support.annotation.StringRes;
import android.util.Log;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.Toast;

import com.eutechpro.android.stanje.BaseActivity;
import com.eutechpro.android.stanje.KamereApplication;
import com.eutechpro.android.stanje.R;
import com.eutechpro.android.stanje.commons.CloseBottomSheetDrawerAction;
import com.eutechpro.android.stanje.commons.DetailsBottomSheet;
import com.eutechpro.android.stanje.data.entities.MapPoint;
import com.eutechpro.android.stanje.firebase_tracking.FirebaseTracker;
import com.eutechpro.android.stanje.maps.GoogleMapManager;
import com.google.android.gms.maps.SupportMapFragment;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CamerasActivity extends BaseActivity implements Mvp.View {
    private static long trackingStartTime;//todo move to presenter
    @BindView(R.id.enable_tracking)
    CheckBox           enableTracking;
    @BindView(R.id.bottom_sheet)
    DetailsBottomSheet detailsBottomSheet;
    @BindView(R.id.camera_warning_view)
    CameraWarningView  cameraWarningView;

    @Inject
    Mvp.Presenter presenter;

    @Inject
    FirebaseTracker firebaseTracker;

    private GoogleMapManager mapManager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Inject
        KamereApplication.get(this).getCamerasActivityComponent().inject(this);

        // Set layout
        setContentView(R.layout.cameras_activity);
        ButterKnife.bind(this);
        super.initToolBarWithLeftDrawer(R.drawable.nav_cameras_icon);
        super.initLeftDrawer();

        // Bottom sheet
        detailsBottomSheet.init();
        setOnDrawerAction(new CloseBottomSheetDrawerAction(detailsBottomSheet));

        // Map
        mapManager = new GoogleMapManager(R.drawable.map_camera_marker);
        mapManager.attachTo((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map));
        mapManager.setOnMapInteractionListener(interactedMapPoint -> presenter.tapOnMapMarker(interactedMapPoint));

        // Tracking
        enableTracking.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked){
                CamerasActivity.trackingStartTime = System.currentTimeMillis();
                presenter.startLocationTracking();
                getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
            } else {
                if (CamerasActivity.trackingStartTime > 0){
                    long duration = System.currentTimeMillis() - CamerasActivity.trackingStartTime;
                    firebaseTracker.logTrackingLength(duration);
                }
                CamerasActivity.trackingStartTime = 0;
                presenter.stopLocationTracking();
                getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
            }
        });
        // Bind
        presenter.bindView(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(CamerasActivity.trackingStartTime > 0) {
            long duration = System.currentTimeMillis() - CamerasActivity.trackingStartTime;
            firebaseTracker.logTrackingLength(duration);
        }
        CamerasActivity.trackingStartTime = 0;
        presenter.stopLocationTracking();
        hideWarningView();
    }

    @Override
    public void onBackPressed() {
        if(detailsBottomSheet.isExpanded()){
            detailsBottomSheet.collapse();
        }else{
            finish();
        }
    }
    @Override
    public void showCameraDetailsView(MapPoint mapPoint) {
        detailsBottomSheet.setTitle(mapPoint.getTitle());
        detailsBottomSheet.setDescription(mapPoint.getDescription());
        detailsBottomSheet.expand();
        firebaseTracker.logCameraDetailsOpening(mapPoint.getTitle());
    }

    @Override
    public void showWarningView(MapPoint mapPoint, int distance) {
        if(cameraWarningView.isVisible()){
            Log.d("TAG", "showWarningView: update");
            cameraWarningView.updateDistance(distance);
        }else{
            Log.d("TAG", "showWarningView: init");
            cameraWarningView.show(mapPoint, distance);
            firebaseTracker.logWarningEvent(mapPoint.getTitle());
        }
    }

    @Override
    public void hideWarningView() {
        cameraWarningView.hide();
    }

    @Override
    public void drawMarkersOnMap(List<MapPoint> cameras) {
        mapManager.clearMapMarkers();
        mapManager.drawMarkersOnMap(cameras);
    }

    @Override
    public void focusMapToLocation(Location location) {
        mapManager.moveToLocationAndKeepZoomLevel(location);
    }

    @Override
    public void showError(@StringRes int message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
    }
}
