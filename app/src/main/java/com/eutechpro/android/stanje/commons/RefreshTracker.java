package com.eutechpro.android.stanje.commons;

import android.content.Context;
import android.content.SharedPreferences;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * For tracking refresh moments of data.
 */
public class RefreshTracker {
//    private static final int REFRESH_THRESHOLD = 20000;
private static final int MINUTE            = 60 * 1000;
private static final int HOUR              = 60 * MINUTE;
private static final int REFRESH_THRESHOLD = 3 * HOUR;//3h
    private final SharedPreferences preferences;

    public RefreshTracker(Context context) {
        preferences = context.getSharedPreferences("refresh_tracker", Context.MODE_PRIVATE);
    }

    /**
     * Store latest refresh time.
     */
    public void saveRefreshTimeStamp() {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putLong("latest_refresh_time", System.currentTimeMillis());
        editor.apply();
    }
    /**
     * Fet latest refresh time.
     *
     * @return latest refresh time. In milliseconds from UNIX era.
     */
    public long getRefreshTimestamp() {
        return preferences.getLong("latest_refresh_time", 0);
    }

    public String getLatestRefreshTimeAsString() {
        Date       date      = new Date(getRefreshTimestamp());
        DateFormat formatter = new SimpleDateFormat("d. MMM. HH:mm", new Locale("sr_Latn_RS"));
        return formatter.format(date);
    }

    /**
     * Check was last refresh time older than set threshold.
     */
    public boolean isTimeForRefresh() {
        return (System.currentTimeMillis() - getRefreshTimestamp()) > REFRESH_THRESHOLD;
    }
}
