package com.eutechpro.android.stanje.border;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;

import com.eutechpro.android.stanje.commons.db.DbHelper;
import com.eutechpro.android.stanje.data.entities.MapPoint;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.schedulers.Schedulers;


class BorderRepositoryDb implements BordersRepository {
    private final DbHelper dbHelper;
    private static final String[] allColumns = {
            DbHelper.ROADWORKS_ID,
            DbHelper.ROADWORKS_TITLE,
            DbHelper.ROADWORKS_DESCRIPTION,
            DbHelper.ROADWORKS_META,
            DbHelper.ROADWORKS_TYPE,
            DbHelper.ROADWORKS_LAT,
            DbHelper.ROADWORKS_LNG,
    };


    BorderRepositoryDb(DbHelper dbHelper) {
        this.dbHelper = dbHelper;
    }

    @Override
    public Observable<List<MapPoint>> getBorderCrossings() {
        return Observable.create((ObservableOnSubscribe<List<MapPoint>>)
                subscriber -> {
                    subscriber.onNext(fetchFromDb());
                    subscriber.onComplete();
        }).subscribeOn(Schedulers.newThread());

    }

    @NonNull
    private List<MapPoint> fetchFromDb() {
        List<MapPoint> mapPoints = new ArrayList<>();

        SQLiteDatabase database = dbHelper.getReadableDatabase();
        Cursor         cursor   = null;
        try {
            cursor = database.query(
                    DbHelper.ROADWORKS_TABLE_NAME,
                    allColumns,
                    DbHelper.CAMERA_TITLE + " LIKE ?", new String[] {"%GP%"},
                    null, null, null);

            cursor.moveToFirst();
            while(!cursor.isAfterLast()) {
                MapPoint roadwork = DbHelper.cursorToRoadworksModel(cursor);
                mapPoints.add(roadwork);
                cursor.moveToNext();
            }
        } finally {
            if(cursor != null) {
                cursor.close();
            }
        }
        return mapPoints;
    }
}
