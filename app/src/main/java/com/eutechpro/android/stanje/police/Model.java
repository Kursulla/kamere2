package com.eutechpro.android.stanje.police;

import android.support.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;


class Model implements Mvp.Model {
    public static final String TAG = "Model";
    private final FirebaseDatabase        database;
    private final DatabaseReference       dbRef;
    private       PublishSubject<Boolean> subject;

    public Model() {
        database = FirebaseDatabase.getInstance();
        dbRef = database.getReference("subscribers_for_patrols").push();
        subject = PublishSubject.create();
    }

    @Override
    public Observable<Boolean> getStream() {
        return subject;
    }

    @Override
    public void subscribe(String email, String name) {
        dbRef.setValue(new Subscriber(email, name))
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        subject.onNext(false);
                    }
                })
                .addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                subject.onNext(task.isSuccessful());
            }
        });
    }

    private static class Subscriber {
        String email;
        String name;

        public Subscriber() {
        }

        public Subscriber(String email, String name) {
            this.email = email;
            this.name = name;
        }
    }
}
