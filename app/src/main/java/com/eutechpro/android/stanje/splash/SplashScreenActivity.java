package com.eutechpro.android.stanje.splash;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import com.eutechpro.android.stanje.KamereApplication;
import com.eutechpro.android.stanje.R;
import com.eutechpro.android.stanje.push.PushNotificationProcessor;
import com.eutechpro.android.stanje.roadworks.RoadworksActivity;
import com.eutechpro.android.stanje.sync.SyncAdapterManager;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SplashScreenActivity extends Activity implements Mvp.View {
    @BindView(R.id.first_time_info)
    TextView     firstTimeInfo;
    @Inject
    MvpPresenter presenter;

    private PushNotificationProcessor pushNotificationProcessor;//todo into presenter


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        KamereApplication.get(this).getSplashScreenComponent().inject(this);

        ButterKnife.bind(this);

        //Setup SyncAdapter for refreshing ApplicationSettings
        SyncAdapterManager.init(this);

        firstTimeInfo.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/roboto_light.ttf"));

        pushNotificationProcessor = new PushNotificationProcessor();

        presenter.bindView(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        if(pushNotificationProcessor.processNotification(getIntent().getExtras(), this)) {
            finish();
            return;
        }
        presenter.startDataRefreshing();
//        proceedToNextScreen();

    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.unBindView();
    }

    @Override
    public void showToast(int message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void proceedToNextScreen() {
        Intent intent = new Intent(SplashScreenActivity.this, RoadworksActivity.class);
        startActivity(intent);
        overridePendingTransition(0, 0);
        finish();
    }
}
