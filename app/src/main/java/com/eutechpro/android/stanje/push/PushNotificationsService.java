package com.eutechpro.android.stanje.push;

import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

public class PushNotificationsService extends FirebaseMessagingService {
    private static final String TAG = "PushNotifService";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Log.d(TAG, "From: " + remoteMessage.getFrom());

        PushNotificationProcessor processor = new PushNotificationProcessor();
        processor.processNotification(remoteMessage, getApplicationContext());
    }
}
