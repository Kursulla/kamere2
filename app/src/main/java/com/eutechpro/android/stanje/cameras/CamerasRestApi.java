package com.eutechpro.android.stanje.cameras;

import com.eutechpro.android.stanje.data.entities.MapPoint;

import java.util.List;

import io.reactivex.Observable;

public interface CamerasRestApi {
    Observable<List<MapPoint>> downloadCameras();
}
