package com.eutechpro.android.stanje.roadworks;

import com.eutechpro.android.stanje.ApplicationDiComponent;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(
        modules = {
                RoadworksPresenterDiModule.class
        },
        dependencies = ApplicationDiComponent.class)
public interface RoadworksDiComponent {
    void inject(RoadworksActivity roadworksActivity);
}
