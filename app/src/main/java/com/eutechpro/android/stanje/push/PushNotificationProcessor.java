package com.eutechpro.android.stanje.push;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import com.google.firebase.messaging.RemoteMessage;

public class PushNotificationProcessor {
    private static final String ACTION           = "click_action";
    private static final String ACTION_OPEN_URL  = "open_url";
    private static final String ACTION_OPEN_PLAY = "open_play";
    private static final String ACTION_JUST_INFO = "just_info";
    private static final String CONTENT          = "content";

    private NotificationHandler notificationHandler;

    public PushNotificationProcessor() {
        this.notificationHandler = new NotificationHandler();
    }

    public boolean processNotification(Bundle bundle, Activity activity) {
        if(bundle == null) {
            return false;
        }
        String action = bundle.getString(PushNotificationProcessor.ACTION);

        if(ACTION_OPEN_URL.equalsIgnoreCase(action)) {
            String url    = bundle.getString(CONTENT);
            Intent intent = new Intent(activity, WebActivity.class);
            intent.putExtra(WebActivity.EXTRAS_KEY_URL, url);
            activity.startActivity(intent);
            activity.finish();
            return true;
        } else if(ACTION_JUST_INFO.equalsIgnoreCase(action)) {

            return true;
        } else if(ACTION_OPEN_PLAY.equalsIgnoreCase(action)) {
            String packageName = bundle.getString(CONTENT);
            if(packageName == null || packageName.isEmpty()) {
                packageName = activity.getPackageName();
            }
            Intent intent = new Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse("market://details?id=" + packageName));
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            activity.startActivity(intent);
            activity.finish();
            return true;
        }
        return false;

    }

    boolean processNotification(RemoteMessage remoteMessage, Context context) {
        if(remoteMessage == null || remoteMessage.getData().isEmpty()) {
            return false;
        }
        String title = remoteMessage.getNotification().getTitle();
        String desc  = remoteMessage.getNotification().getBody();

        String action = remoteMessage.getData().get(ACTION);

        if(action.equalsIgnoreCase(ACTION_OPEN_URL)) {
            String url = remoteMessage.getData().get(CONTENT);
            if(url == null || url.isEmpty()) {
                return false;
            }
            notificationHandler.fireWebNotification(title, desc, url, context.getApplicationContext());

            return true;
        } else if(ACTION_JUST_INFO.equalsIgnoreCase(action)) {

            return true;
        } else if(ACTION_OPEN_PLAY.equalsIgnoreCase(action)) {
            String packageName = remoteMessage.getData().get(CONTENT);
            if(packageName == null || packageName.isEmpty()) {
                packageName = context.getPackageName();
            }
            notificationHandler.firePlayNotification(title, desc, packageName, context.getApplicationContext());
            return true;
        }
        return false;
    }
}
