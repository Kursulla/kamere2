package com.eutechpro.android.stanje;

import android.content.Context;

import com.eutechpro.android.stanje.firebase_tracking.FirebaseDiModule;
import com.eutechpro.android.stanje.firebase_tracking.FirebaseTracker;

import dagger.Component;


@Component(modules = {
        ApplicationDiModule.class,
        FirebaseDiModule.class
})
public interface ApplicationDiComponent {
    Context getContext();
    FirebaseTracker getFirebaseTracker();
}
