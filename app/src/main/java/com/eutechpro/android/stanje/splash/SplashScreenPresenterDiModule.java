package com.eutechpro.android.stanje.splash;

import android.content.Context;

import com.eutechpro.android.stanje.cameras.CamerasRepository;
import com.eutechpro.android.stanje.cameras.CamerasRepositoryDb;
import com.eutechpro.android.stanje.cameras.CamerasRestApi;
import com.eutechpro.android.stanje.cameras.CamerasRetrofitApiClient;
import com.eutechpro.android.stanje.commons.RefreshTracker;
import com.eutechpro.android.stanje.commons.db.DbHelper;
import com.eutechpro.android.stanje.roadworks.RoadworksDbRepository;
import com.eutechpro.android.stanje.roadworks.RoadworksRepository;
import com.eutechpro.android.stanje.roadworks.RoadworksRestApi;
import com.eutechpro.android.stanje.roadworks.RoadworksRetrofitApiClient;

import dagger.Module;
import dagger.Provides;

@Module
public class SplashScreenPresenterDiModule {
    @Provides
    MvpPresenter providesPresenter(Context context){

        CamerasRestApi      camerasApi     = new CamerasRetrofitApiClient();
        CamerasRepository   camerasRepo    = new CamerasRepositoryDb(DbHelper.getDbHelperInstance(context));
        RoadworksRestApi    roadworksApi   = new RoadworksRetrofitApiClient();
        RoadworksRepository roadworksRepo  = new RoadworksDbRepository(DbHelper.getDbHelperInstance(context));
        RefreshTracker      refreshTracker = new RefreshTracker(context);

        return new MvpPresenter(new MvpModel(camerasApi, camerasRepo, roadworksApi, roadworksRepo, refreshTracker));
    }
}
