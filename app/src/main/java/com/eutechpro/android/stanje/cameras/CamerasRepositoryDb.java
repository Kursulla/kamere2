package com.eutechpro.android.stanje.cameras;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;

import com.eutechpro.android.stanje.BuildConfig;
import com.eutechpro.android.stanje.commons.db.DbHelper;
import com.eutechpro.android.stanje.data.entities.MapPoint;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.schedulers.Schedulers;


public class CamerasRepositoryDb implements CamerasRepository {
    private final DbHelper dbHelper;
    private static final String[] allColumns = {
            DbHelper.CAMERA_TITLE,
            DbHelper.CAMERA_DESCRIPTION,
            DbHelper.CAMERA_META,
            DbHelper.CAMERA_TYPE,
            DbHelper.CAMERA_IMAGE,
            DbHelper.CAMERA_LAT,
            DbHelper.CAMERA_LNG,
    };

    public CamerasRepositoryDb(DbHelper dbHelper) {
        this.dbHelper = dbHelper;
    }

    @Override
    public Observable<List<MapPoint>> getAllCameras() {
        return Observable.create((ObservableOnSubscribe<List<MapPoint>>)
                subscriber -> {
                    List<MapPoint> cameras = getCamerasFromDb();
                    if(BuildConfig.DEBUG){
                        cameras.add(new MapPoint(9990, "prvi", "52.5328085", "13.4207062"));
                        cameras.add(new MapPoint(9991, "drugi", "52.5317751", "13.4183135"));
                        cameras.add(new MapPoint(9992, "treci", "52.5306722", "13.4167253"));
                        cameras.add(new MapPoint(9993, "total", "52.5294777", "13.4157063"));
                        cameras.add(new MapPoint(9994, "fitX", "52.5212347", "13.4112524"));
                        cameras.add(new MapPoint(9995, "rotes rathaus", "52.5169382", "13.4070393"));
                        cameras.add(new MapPoint(9996, "raskrsnica sa ringier", "52.511247", "13.401175"));
                        cameras.add(new MapPoint(9997, "suzenje", "52.510430", "13.391519"));
                        cameras.add(new MapPoint(991, "lajpciger plac", "52.509627", "13.378323"));
                        cameras.add(new MapPoint(992, "lajpciger plac", "52.509627", "13.378323"));
                        cameras.add(new MapPoint(993, "radovi na mosticu", "52.506094", "13.368485"));
                        cameras.add(new MapPoint(994, "stan", "52.525501", "13.418877"));

                        cameras.add(new MapPoint(995, "kod stiftunga", "52.503822", "13.352789"));
                        cameras.add(new MapPoint(996, "Ambrosius", "52.502255", "13.353197"));
                        cameras.add(new MapPoint(997, "nollendorf", "52.499544", "13.354044"));

                        cameras.add(new MapPoint(998, "cosak kod neta", "52.523125","13.425915"));
                        cameras.add(new MapPoint(899, "vivantes", "52.523960","13.439288"));
                        cameras.add(new MapPoint(890, "landels", "52.528533","13.458001"));
                        cameras.add(new MapPoint(891, "skretanje ka joe", "52.533098","13.476248"));
                        cameras.add(new MapPoint(892, "Skretanje ka pranju", "52.534265","13.499623"));
                        cameras.add(new MapPoint(893, "baumarkt", "52.534115","13.510277"));

//                    52.5212347,13.4112524 - fitx
//                    52.5169382,13.4070393 rotes rathaus
//                    52.511247, 13.401175 raskrsnica sa ringer
//                    52.510430, 13.391519 suzenje
//                    52.509627, 13.378323 lajpciger plac
//                    52.506094, 13.368485 radovi na mosticu
                    }
                    if (cameras.isEmpty()){
                        subscriber.onError(new NoCamerasInDbException());
                    } else {
                        subscriber.onNext(cameras);
                    }
                    subscriber.onComplete();
        }).subscribeOn(Schedulers.newThread());

    }

    @Override
    public Observable<Boolean> saveCameraPoint(final MapPoint mapPoint) {
        return Observable.create(subscriber -> {
            long savedEntryId = save(mapPoint);
            if(savedEntryId == -1) {
                subscriber.onNext(false);
            } else {
                subscriber.onNext(true);
            }
            subscriber.onComplete();
        });
    }


    @Override
    public Observable<Boolean> saveCamerasInBulk(final List<MapPoint> cameras) {
        return Observable.create(subscriber -> {
            int numberOfSavedItems = 0;

            for(MapPoint mapPoint : cameras) {
                if(save(mapPoint) != -1) {
                    numberOfSavedItems++;
                }
            }
            if(numberOfSavedItems == cameras.size()) {
                subscriber.onNext(true);
            } else {
                subscriber.onNext(false);
            }
            subscriber.onComplete();
        });

    }

    @Override
    public Observable<Boolean> removeAllCameras() {
        return Observable.create(subscriber -> {
            SQLiteDatabase database     = dbHelper.getReadableDatabase();
            database.delete(DbHelper.CAMERA_TABLE_NAME, "1", null);
            subscriber.onNext(true);
            subscriber.onComplete();
        });
    }

    @Override
    public Observable<Boolean> replaceAllCameras(List<MapPoint> cameras) {
        return removeAllCameras()
                .flatMap(removed -> {
                    if (removed){
                        return saveCamerasInBulk(cameras);
                    } else {
                        return Observable.just(false);
                    }
                });
    }

    private long save(MapPoint mapPoint) {
        SQLiteDatabase database      = dbHelper.getWritableDatabase();
        ContentValues  contentValues = DbHelper.cameraModelToContentValues(mapPoint);
        return database.insertWithOnConflict(DbHelper.CAMERA_TABLE_NAME, null, contentValues, SQLiteDatabase.CONFLICT_REPLACE);
    }

    @NonNull
    private List<MapPoint> getCamerasFromDb() {
        List<MapPoint> cameras  = new ArrayList<>();
        SQLiteDatabase database = dbHelper.getReadableDatabase();
        Cursor         cursor   = null;
        try {
            cursor = database.query(DbHelper.CAMERA_TABLE_NAME,//from
                    allColumns,                       //what
                    null, null, null, null, null);

            cursor.moveToFirst();
            while(!cursor.isAfterLast()) {
                MapPoint mapPoint = DbHelper.cursorToCameraModel(cursor);
                cameras.add(mapPoint);
                cursor.moveToNext();
            }
        } finally {
            if(cursor != null) {
                cursor.close();
            }
        }

        return cameras;
    }

}
