package com.eutechpro.android.stanje.splash;

import android.util.Log;

import com.eutechpro.android.stanje.BuildConfig;
import com.eutechpro.android.stanje.R;
import com.google.firebase.messaging.FirebaseMessaging;

import java.net.UnknownHostException;

import io.reactivex.android.schedulers.AndroidSchedulers;

class MvpPresenter implements Mvp.Presenter {
    private static final String TAG            = "MvpPresenter";
    private Mvp.View  view;
    private Mvp.Model model;

    MvpPresenter(Mvp.Model model) {
        this.model = model;
    }

    @Override
    public void bindView(Mvp.View view) {
        this.view = view;
        FirebaseMessaging.getInstance().subscribeToTopic("all");
        if(BuildConfig.DEBUG) {
            FirebaseMessaging.getInstance().subscribeToTopic("debug");
        } else {
            FirebaseMessaging.getInstance().unsubscribeFromTopic("debug");
        }

        model.dataStream()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        fetchedResult -> {
                            if (fetchedResult.getException() != null){
                                if (fetchedResult.getException() instanceof UnknownHostException){
                                    view.showToast(R.string.splash_toast_failed_to_load_data_internet);
                                } else {
                                    view.showToast(R.string.splash_toast_failed_to_load_data);
                                }
                            } else {
                                if (fetchedResult.cameras.isEmpty()){
                                    view.showToast(R.string.splash_toast_partial_data_loaded);
                                }
                                if (fetchedResult.roadWorks.isEmpty()){
                                    view.showToast(R.string.splash_toast_partial_data_loaded);
                                }
                            }
                            view.proceedToNextScreen();
                        }, throwable -> {
                            if (throwable instanceof UnknownHostException){
                                view.showToast(R.string.splash_toast_failed_to_load_data_internet);
                            } else {
                                view.showToast(R.string.splash_toast_failed_to_load_data);
                            }
                        });
    }

    @Override
    public void unBindView() {
        this.view = null;
    }

    @Override
    public void startDataRefreshing() {
        if (!model.isTimeForRefresh()){
            Log.d(TAG, "startDataRefreshing: Not time for refresh");
            view.proceedToNextScreen();
            return;
        }
        Log.d(TAG, "startDataRefreshing: It is time for refresh");
        model.refreshData();
    }

}
