package com.eutechpro.android.stanje.about;

import android.graphics.Typeface;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.TextView;

import com.eutechpro.android.stanje.BaseActivity;
import com.eutechpro.android.stanje.BuildConfig;
import com.eutechpro.android.stanje.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AboutActivity extends BaseActivity {
    @BindView(R.id.about_title)
    TextView title;
    @BindView(R.id.about_description)
    TextView description;
    @BindView(R.id.about_app_version)
    TextView appVersion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.about_activity_layout);
        ButterKnife.bind(this);
        super.initToolBarWithBack();

        setFont();

        appVersion.setText(getString(R.string.about_version, BuildConfig.VERSION_NAME, BuildConfig.VERSION_CODE));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home){
            onBackPressed();
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    private void setFont() {
        Typeface typefaceWithFont = Typeface.createFromAsset(getAssets(), "fonts/roboto_light.ttf");
        title.setTypeface(typefaceWithFont);
        description.setTypeface(typefaceWithFont);
        appVersion.setTypeface(typefaceWithFont);
    }
}
