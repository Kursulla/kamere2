package com.eutechpro.android.stanje.police;


import io.reactivex.functions.Consumer;

public class Presenter implements Mvp.Presenter {
    private Mvp.View  view;
    private Mvp.Model model;

    public Presenter(Mvp.Model model) {
        this.model = model;
    }

    @Override
    public void bindView(Mvp.View view) {
        this.view = view;
        model.getStream().subscribe(new Consumer<Boolean>() {
            @Override
            public void accept(Boolean aBoolean) throws Exception {
                if (aBoolean){
                    view.sendingSuccessful();
                } else {
                    view.sendingUnSuccessful();
                }
            }
        });
    }

    @Override
    public void unBind() {
        this.view = null;
    }

    @Override
    public void subscribe(String email, String name) {
        model.subscribe(email, name);
    }
}
