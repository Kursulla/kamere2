package com.eutechpro.android.stanje.sync;

import android.accounts.Account;
import android.content.AbstractThreadedSyncAdapter;
import android.content.ContentProviderClient;
import android.content.Context;
import android.content.SyncResult;
import android.os.Bundle;
import android.util.Log;

import com.eutechpro.android.stanje.cameras.CamerasRepository;
import com.eutechpro.android.stanje.cameras.CamerasRepositoryDb;
import com.eutechpro.android.stanje.cameras.CamerasRetrofitApiClient;
import com.eutechpro.android.stanje.commons.RefreshTracker;
import com.eutechpro.android.stanje.commons.db.DbHelper;
import com.eutechpro.android.stanje.roadworks.RoadworksDbRepository;
import com.eutechpro.android.stanje.roadworks.RoadworksRetrofitApiClient;

/**
 * The real magic happens here.
 * <br/>
 * This is adapter that will be called when system decide it is time to sync data.
 * <br/>
 * Created by Kursulla on 07/09/15.
 */
class SyncAdapter extends AbstractThreadedSyncAdapter {
    private static final String TAG = "SyncAdapter";
    private final RefreshTracker refreshTracker;

    SyncAdapter(Context context) {
        super(context, true);
        refreshTracker = new RefreshTracker(context);
    }

    @Override
    public void onPerformSync(Account account, Bundle extras, String authority, ContentProviderClient contentProviderClient, SyncResult syncResult) {
        Log.d(TAG, "******* onPerformSync *******");
        if(!syncResult.syncAlreadyInProgress) {
            CamerasRetrofitApiClient camerasApiClient = new CamerasRetrofitApiClient();
            camerasApiClient
                    .downloadCameras()
                    .subscribe(
                            cameras -> {
                                if (cameras != null && !cameras.isEmpty()){
                                    CamerasRepository camerasRepository = new CamerasRepositoryDb(DbHelper.getDbHelperInstance(getContext()));
                                    camerasRepository
                                            .replaceAllCameras(cameras)
                                            .subscribe(replaced -> {
                                                Log.d(TAG, "Cameras refreshed: "+replaced);
                                                if (replaced){
                                                    if (refreshTracker != null)
                                                        refreshTracker.saveRefreshTimeStamp();
                                                }
                                            });
                                }
                            }

                    );

            RoadworksRetrofitApiClient roadworksApiClient = new RoadworksRetrofitApiClient();
            roadworksApiClient
                    .downloadAllRoadworks()
                    .subscribe(roadworks -> {
                        if (roadworks != null && !roadworks.isEmpty()){
                            RoadworksDbRepository roadworksRepository = new RoadworksDbRepository(DbHelper.getDbHelperInstance(getContext()));
                            roadworksRepository
                                    .replaceAllRoadworks(roadworks)
                                    .subscribe(replaced -> {
                                        Log.d(TAG, "Roadworks refreshed: "+replaced);
                                        if (replaced){
                                            if (refreshTracker != null)
                                                refreshTracker.saveRefreshTimeStamp();
                                        }
                                    });
                        }
                    });

        }
    }
}