# Notifications

Each notification has 3 mandatory parts:
- Title: text that will be shown as title of notification
- Message text: Bigger, explanatory, text of the notification
- Topic: it is important to set __debug__ if we are testing. Otherwie all users will get our trash messages! Not opic means all users.

We have 2 types of notifications:
1. Open GooglePlay (Usually for updates of the app)
2. Open web url
3. Simple textual information


##### 1. Open GooglePlay

We are sneding 2 parameters:
- _click_action_ -> __open_play__
- _content_ -> __package of the application we want to open (com.eutechpro.android.stanje)__



##### 2. Open web url

We are sneding 2 parameters:

- _click_action_ -> __open_url__
- _content_ -> __URL to the page we want to be opened (http://www.google.com)__

##### 3. Simple textual information

We are sneding 2 parameters:

- _click_action_ -> __just_info__


