package com.eutechpro.android.stanje.maps.location;

import android.location.Location;

public interface SimpleLocationManager {
    void connect();

    void disconnect();

    void stopLocationUpdates();

    void startLocationUpdates();

    @SuppressWarnings("unused")
    void setLocationChangesListener(LocationChangesListener locationChangesListener);

    @SuppressWarnings("unused")
    void setLocationManagerListener(LocationManagerListener locationManagerListener);


    //region Callback interfaces
    /**
     * Callback interface just for tracking location changes.
     */
    interface LocationChangesListener {
        void locationChanged(Location location);
    }

    /**
     * Callback interface for tracking full cycle of location manager life.
     */
    interface LocationManagerListener {
        void locationChanged(Location location);

        void connected();

        void suspended();

        void failed();
    }
    //endregion
}
