package com.eutechpro.android.stanje.commons;

import android.content.res.AssetManager;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class AssetsReader {
    private AssetManager assetManager;

    public AssetsReader(AssetManager assetManager) {
        this.assetManager = assetManager;
    }

    public String readAssetFrom(String path) {
        String         contents = "";
        InputStream    is       = null;
        BufferedReader reader   = null;
        try {
            is = assetManager.open(path);
            reader = new BufferedReader(new InputStreamReader(is));
            contents = reader.readLine();
            String line;
            while((line = reader.readLine()) != null) {
                contents += '\n' + line;
            }
        } catch(final Exception e) {
            e.printStackTrace();
        } finally {
            if(is != null) {
                try {
                    is.close();
                } catch(IOException ignored) {
                }
            }
            if(reader != null) {
                try {
                    reader.close();
                } catch(IOException ignored) {
                }
            }
        }
        return contents;
    }
}
