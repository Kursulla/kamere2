package com.eutechpro.android.stanje.roadworks;

import com.eutechpro.android.stanje.data.entities.MapPoint;

import java.util.List;

import io.reactivex.Observable;

public interface RoadworksRepository {
    Observable<Boolean> saveRoadworkRx(MapPoint roadworkToSave);

    Observable<Boolean> saveRoadworksInBulkRx(List<MapPoint> roadworks);

    Observable<List<MapPoint>> getRoadworksRx();

    Observable<Boolean> removeAllRoadworksRx();

    Observable<Boolean> replaceAllRoadworks(List<MapPoint> roadworks);
}
