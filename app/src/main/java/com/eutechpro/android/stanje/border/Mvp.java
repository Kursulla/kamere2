package com.eutechpro.android.stanje.border;

import android.location.Location;
import android.support.annotation.StringRes;

import com.eutechpro.android.stanje.BaseMvpPresenter;
import com.eutechpro.android.stanje.BaseMvpView;
import com.eutechpro.android.stanje.data.entities.MapPoint;

import java.util.List;

import io.reactivex.Observable;


interface Mvp {
    interface Model {
        Observable<List<MapPoint>> getBorderCrossing();
    }

    interface View extends BaseMvpView{
        /**
         * Draw list of {@link MapPoint} on the map.
         *
         * @param cameras list of cameras
         */
        void drawMarkersOnMap(List<MapPoint> cameras);

        /**
         * Show bottom sheet with information for selected {@link MapPoint}
         */
        void showRoadworkDetails(MapPoint mapPoint);

        /**
         * Focus map on specified {@link Location}
         */
        void focusMapToLocation(Location location);

        void showError(@StringRes int message);
    }

    interface Presenter extends BaseMvpPresenter<View>{
        /**
         * Event that will be called when user select specified {@link MapPoint}.
         */
        void tapOnMapMarker(MapPoint mapPoint);
    }
}
