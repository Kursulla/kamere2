package com.eutechpro.android.stanje.fines;

import com.eutechpro.android.stanje.R;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;


class MvpPresenter implements Mvp.Presenter {
    private Mvp.Model           model;
    private Mvp.View            view;
    private CompositeDisposable subscriptions;

    MvpPresenter(Mvp.Model model) {
        this.model = model;
        this.subscriptions = new CompositeDisposable();
    }


    @Override
    public void bindView(Mvp.View view) {
        this.view = view;
        Disposable subscription = model.getFinesByQuery("")
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        fines1 -> {
                            if (view == null) return;
                            view.showInitData(fines1);
                        },
                        throwable -> {
                            if (view == null) return;
                            view.showError(R.string.fines_toast_error_unable_to_load);
                        }
                );
        subscriptions.add(subscription);
    }

    @Override
    public void unBindView() {
        view = null;
        subscriptions.clear();
    }


    @Override
    public void filterFines(final String searchString) {
        model.getFinesByQuery(searchString)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        fines -> {
                            if (view == null) return;
                            view.showFilteredData(fines);
                        },
                        throwable -> {
                            if (view == null) return;
                            view.showError(R.string.fines_toast_error_unable_to_filter);
                        }
                );

    }
}
