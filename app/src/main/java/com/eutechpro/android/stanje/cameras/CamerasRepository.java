package com.eutechpro.android.stanje.cameras;

import com.eutechpro.android.stanje.data.entities.MapPoint;

import java.util.List;

import io.reactivex.Observable;


/**
 * Repository for getting cameras. It might have various implementations
 * <br/> with different underlying layers (Retrofit, SqLite, Realm, Firebase....)
 */
public interface CamerasRepository {
    Observable<List<MapPoint>> getAllCameras();

    Observable<Boolean> saveCameraPoint(MapPoint mapPoint);

    Observable<Boolean> saveCamerasInBulk(List<MapPoint> cameras);

    Observable<Boolean> removeAllCameras();

    Observable<Boolean> replaceAllCameras(List<MapPoint> cameras);
}
