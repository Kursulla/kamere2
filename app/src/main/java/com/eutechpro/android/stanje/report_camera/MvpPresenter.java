package com.eutechpro.android.stanje.report_camera;

import android.util.Log;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;


class MvpPresenter implements Mvp.Presenter {
    private static final String TAG = "MvpPresenter";
    private Mvp.View            view;
    private Mvp.Model           model;
    private CompositeDisposable subscriptions;

    MvpPresenter(Mvp.Model model) {
        this.model = model;
        this.subscriptions = new CompositeDisposable();
    }


    @Override
    public void bindView(Mvp.View view) {
        this.view = view;
    }

    @Override
    public void unBindView() {
        this.view = null;
        this.subscriptions.clear();
    }

    @Override
    public void sendMessage(String email, String message) {
        viewGuard();
        Disposable subscription = model.reportCamera(email, message)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        status -> {
                            Log.d(TAG, "reportCameraPresenter::onNext: MvpPresenter=" + status);
                            if (view == null){
                                return;
                            }
                            if(status){
                                view.messageSent();
                            }else{
                                view.messageFailed();
                            }
                        },
                        throwable -> {
                            Log.d(TAG, "reportCameraPresenter::onError: " + throwable.getLocalizedMessage());
                            if (view == null){
                                return;
                            }
                            view.messageFailed();
                        });
        subscriptions.add(subscription);
    }

    private void viewGuard() {
        if (view == null){
            throw new IllegalStateException("You have to bind view!!!");
        }
    }
}
