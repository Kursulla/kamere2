package com.eutechpro.android.stanje.fines;

import android.content.Context;

import com.eutechpro.android.stanje.commons.AssetsReader;

import dagger.Module;
import dagger.Provides;

@Module
public class FinesPresenterDiModule {
    @Provides
    Mvp.Presenter providesPresenter(Context context) {
        Mvp.Model model = new MvpModel(new FinesAssetRepository(new AssetsReader(context.getAssets()), "fines/kazne.json"));
        return new MvpPresenter(model);
    }
}
