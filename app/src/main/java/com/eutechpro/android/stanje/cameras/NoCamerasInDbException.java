package com.eutechpro.android.stanje.cameras;

/**
 * Exception in case there are no items in database.
 */
class NoCamerasInDbException extends Exception {
    NoCamerasInDbException() {
        super("There are no Cameras in local database! It is either empty or something went wrong!!!");
    }
}
