package com.eutechpro.android.stanje.cameras;

import android.content.Context;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.eutechpro.android.stanje.R;
import com.eutechpro.android.stanje.data.entities.MapPoint;

import butterknife.BindView;
import butterknife.ButterKnife;


public class CameraWarningView extends LinearLayout {
    @BindView(R.id.camera_distance)
    TextView cameraDistance;
    @BindView(R.id.camera_location)
    TextView cameraLocation;
    @BindView(R.id.camera_description)
    TextView cameraDescription;
    @BindView(R.id.camera_type)
    ImageView cameraType;

    public CameraWarningView(Context context, AttributeSet attrs) {
        super(context, attrs);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (inflater == null){
            return;
        }
        inflater.inflate(R.layout.camera_warning_view, this, true);

        ButterKnife.bind(this);

        setVisibility(GONE);
    }

    /**
     * Show warning view with appropriate {@link MapPoint} and distance to camera.
     *
     * @param mapPoint mapPoint of a warning
     * @param distanceToCamera distance to mapPoint
     */
    public void show(MapPoint mapPoint, int distanceToCamera) {
        cameraDistance.setText(getContext().getString(R.string.camera_warning_view_distance, distanceToCamera));
        cameraLocation.setText(mapPoint.getTitle());
        cameraDescription.setText(mapPoint.getDescription());
        playAudioNotif();
        setVisibility(VISIBLE);
    }


    public void updateDistance(int distanceToCamera) {
        cameraDistance.setText(getContext().getString(R.string.camera_warning_view_distance, distanceToCamera));
    }
    /**
     * Hive warning view.
     */
    public void hide() {
        setVisibility(GONE);
    }

    /** Check is this view shown or not. */
    public boolean isVisible() {
        return getVisibility() == VISIBLE;
    }

    private void playAudioNotif() {
        try {
            Uri      notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            Ringtone r            = RingtoneManager.getRingtone(getContext(), notification);
            r.play();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
