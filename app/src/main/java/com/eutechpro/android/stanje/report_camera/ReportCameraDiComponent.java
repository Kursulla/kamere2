package com.eutechpro.android.stanje.report_camera;


import com.eutechpro.android.stanje.ApplicationDiComponent;

import dagger.Component;

@Component(
        modules = {
            ReportCameraDiModule.class
        },
        dependencies = ApplicationDiComponent.class)
public interface ReportCameraDiComponent {
    void inject(ReportCameraActivity reportCameraActivity);

}
