package com.eutechpro.android.stanje.fines;

import com.eutechpro.android.stanje.BaseMvpPresenter;
import com.eutechpro.android.stanje.BaseMvpView;

import java.util.List;

import io.reactivex.Observable;


interface Mvp {
    interface Model {
        Observable<List<Fine>> getFinesByQuery(String query);
    }

    interface View extends BaseMvpView {
        /**
         * Show initial list of fines
         */
        void showInitData(List<Fine> allFines);

        /**
         * Show fines after filtering
         */
        void showFilteredData(List<Fine> filteredFines);

        /**
         * Show details of selected Fine
         *
         * @param fine you want to show details about
         */
        void showFineDetailsView(Fine fine);

        /**
         * Hide bottom sheet
         */
        void hideFineDetailsView();

        /**
         * Check is Fine's details view shown
         *
         * @return is it shown or not
         */
        boolean isFineDetailsViewShown();
    }

    interface Presenter extends BaseMvpPresenter<View> {
        /**
         * Filter fines based on string query.
         */
        void filterFines(String searchString);
    }
}
