package com.eutechpro.android.stanje;

import android.app.Application;
import android.content.Context;
import android.os.StrictMode;
import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.eutechpro.android.stanje.border.BorderDiComponent;
import com.eutechpro.android.stanje.border.BorderPresenterDiModule;
import com.eutechpro.android.stanje.border.DaggerBorderDiComponent;
import com.eutechpro.android.stanje.cameras.CamerasDiComponent;
import com.eutechpro.android.stanje.cameras.CamerasPresenterDiModule;
import com.eutechpro.android.stanje.cameras.DaggerCamerasDiComponent;
import com.eutechpro.android.stanje.fines.DaggerFinesDiComponent;
import com.eutechpro.android.stanje.fines.FinesDiComponent;
import com.eutechpro.android.stanje.fines.FinesPresenterDiModule;
import com.eutechpro.android.stanje.firebase_tracking.FirebaseDiModule;
import com.eutechpro.android.stanje.report_camera.DaggerReportCameraDiComponent;
import com.eutechpro.android.stanje.report_camera.ReportCameraDiComponent;
import com.eutechpro.android.stanje.report_camera.ReportCameraDiModule;
import com.eutechpro.android.stanje.roadworks.DaggerRoadworksDiComponent;
import com.eutechpro.android.stanje.roadworks.RoadworksDiComponent;
import com.eutechpro.android.stanje.roadworks.RoadworksPresenterDiModule;
import com.eutechpro.android.stanje.splash.DaggerSplashScreenDiComponent;
import com.eutechpro.android.stanje.splash.SplashScreenDiComponent;
import com.eutechpro.android.stanje.splash.SplashScreenPresenterDiModule;
import com.eutechpro.android.stanje.sync.SyncAdapterManager;
import com.facebook.stetho.Stetho;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import io.fabric.sdk.android.Fabric;

public class KamereApplication extends Application {
    private ApplicationDiComponent applicationDiComponent;

    private CamerasDiComponent      camerasActivityComponent;
    private RoadworksDiComponent    roadworksDiComponent;
    private BorderDiComponent       borderDiComponent;
    private SplashScreenDiComponent splashScreenComponent;
    private FinesDiComponent        finesComponent;
    private ReportCameraDiComponent reportCameraComponent;

    @Override
    public void onCreate() {
        if (BuildConfig.DEBUG) {
            StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
                    .detectAll()
                    .build());
            StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder()
                    .detectAll()
                    .build());
        }
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        Stetho.initializeWithDefaults(this);

        SyncAdapterManager.init(getApplicationContext());



        applicationDiComponent = DaggerApplicationDiComponent.builder()
                .applicationDiModule(new ApplicationDiModule(this))
                .firebaseDiModule(new FirebaseDiModule(this))
                .build();
    }

    public static KamereApplication get(Context context) {
        return (KamereApplication) context.getApplicationContext();
    }

    public SplashScreenDiComponent getSplashScreenComponent() {
        if (splashScreenComponent == null){
            splashScreenComponent = DaggerSplashScreenDiComponent
                    .builder()
                    .applicationDiComponent(applicationDiComponent)
                    .splashScreenPresenterDiModule(new SplashScreenPresenterDiModule())
                    .build();
        }
        return splashScreenComponent;
    }

    public RoadworksDiComponent getRoadworksDiComponent() {
        if (roadworksDiComponent == null){
            roadworksDiComponent = DaggerRoadworksDiComponent
                    .builder()
                    .applicationDiComponent(applicationDiComponent)
                    .roadworksPresenterDiModule(new RoadworksPresenterDiModule())
                    .build();
        }
        return roadworksDiComponent;
    }

    public BorderDiComponent getBorderDiComponent() {
        if (borderDiComponent == null){
            borderDiComponent = DaggerBorderDiComponent
                    .builder()
                    .applicationDiComponent(applicationDiComponent)
                    .borderPresenterDiModule(new BorderPresenterDiModule())
                    .build();
        }
        return borderDiComponent;
    }

    public CamerasDiComponent getCamerasActivityComponent() {
        if (camerasActivityComponent == null){
            camerasActivityComponent = DaggerCamerasDiComponent
                    .builder()
                    .applicationDiComponent(applicationDiComponent)
                    .camerasPresenterDiModule(new CamerasPresenterDiModule())
                    .build();
        }
        return camerasActivityComponent;
    }

    public FinesDiComponent getFinesComponent() {
        if (finesComponent == null){
            finesComponent = DaggerFinesDiComponent
                    .builder()
                    .applicationDiComponent(applicationDiComponent)
                    .finesPresenterDiModule(new FinesPresenterDiModule())
                    .build();
        }
        return finesComponent;
    }

    public void setReportCameraDiComponent(ReportCameraDiComponent reportCameraDiComponent) {
        this.reportCameraComponent = reportCameraDiComponent;
    }

    public ReportCameraDiComponent getReportCameraComponent() {
        if (reportCameraComponent == null){
            reportCameraComponent = DaggerReportCameraDiComponent
                    .builder()
                    .applicationDiComponent(applicationDiComponent)
                    .reportCameraDiModule(new ReportCameraDiModule())
                    .build();
        }
        return reportCameraComponent;
    }
}
