package com.eutechpro.android.stanje.report_camera;

import io.reactivex.Observable;

class MvpModel implements Mvp.Model {
    private UserCommunicationService service;

    MvpModel(UserCommunicationService service) {
        this.service = service;
    }

    @Override
    public Observable<Boolean> reportCamera(String email, String message) {
        return service.reportCamera(email, message);
    }
}
