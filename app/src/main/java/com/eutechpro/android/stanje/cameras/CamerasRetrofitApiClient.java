package com.eutechpro.android.stanje.cameras;

import com.eutechpro.android.stanje.data.entities.MapPoint;
import com.eutechpro.android.stanje.data.rest.RetrofitFactory;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.http.GET;

public class CamerasRetrofitApiClient implements CamerasRestApi {
    private static final String CAMERAS_BASE_URL = "http://eutechpro.com";
    private RetrofitInterface retrofitRestInterface;

    public CamerasRetrofitApiClient() {
        retrofitRestInterface = RetrofitFactory
                .generateRetrofit(CAMERAS_BASE_URL)
                .create(RetrofitInterface.class);
    }

    @Override
    public Observable<List<MapPoint>> downloadCameras() {
        return retrofitRestInterface.downloadCameras().subscribeOn(Schedulers.newThread());
    }


    private interface RetrofitInterface {
        @GET("cameras.json")
        Observable<List<MapPoint>> downloadCameras();
    }
}
