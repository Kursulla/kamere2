package com.eutechpro.android.stanje.border;


import android.content.Context;

import com.eutechpro.android.stanje.commons.db.DbHelper;

import dagger.Module;
import dagger.Provides;

@SuppressWarnings("ALL")
@Module
public class BorderPresenterDiModule {
    @Provides
    Mvp.Presenter providesBorderPresenter(Context context) {
        BordersRepository bordersRepository = new BorderRepositoryDb(DbHelper.getDbHelperInstance(context));
        Mvp.Model         model             = new MvpModel(bordersRepository);
        return new MvpPresenter(model);
    }
}
