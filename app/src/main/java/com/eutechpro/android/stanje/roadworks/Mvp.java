package com.eutechpro.android.stanje.roadworks;

import android.location.Location;
import android.support.annotation.StringRes;

import com.eutechpro.android.stanje.BaseMvpPresenter;
import com.eutechpro.android.stanje.data.entities.MapPoint;

import java.util.List;

import io.reactivex.Observable;

public interface Mvp {
    interface Model {
        Observable<List<MapPoint>> getAllRoadWorksRx();
    }

    interface View {
        /**
         * Draw list of {@link MapPoint} on the map.
         */
        void drawMarkersOnMap(List<MapPoint> cameras);

        /**
         * Show bottom sheets for selected {@link MapPoint}
         *
         * @param mapPoint that user selected.
         */
        void showBottomSheet(MapPoint mapPoint);

        /**
         * Hide bottom sheet
         */
        void hideBottomSheet();
        /**
         * Focus map on specified {@link Location}
         */
        void focusMapToLocation(Location location);

        void showError(@StringRes int errorMessageRes);
    }

    interface Presenter extends BaseMvpPresenter<View> {
        /**
         * Event that will be called when user select specified {@link MapPoint}.
         */
        void tapOnMapMarker(MapPoint mapPoint);
    }
}
