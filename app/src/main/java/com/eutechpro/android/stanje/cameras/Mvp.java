package com.eutechpro.android.stanje.cameras;

import android.location.Location;

import com.eutechpro.android.stanje.BaseMvpPresenter;
import com.eutechpro.android.stanje.BaseMvpView;
import com.eutechpro.android.stanje.data.entities.MapPoint;

import java.util.List;

import io.reactivex.Observable;

interface Mvp {
    interface Model{
        Observable<List<MapPoint>> fetchCameras();

        List<MapPoint> getCachedCameras();

        void cacheCameras(List<MapPoint> camerasToCache);
    }

    interface View extends BaseMvpView {
        /**
         * Show bottom sheets for selected {@link MapPoint}
         *
         * @param mapPoint that user selected.
         */
        void showCameraDetailsView(MapPoint mapPoint);

        /**
         * Show appropriate warning view.
         *
         * @param mapPoint mapPoint that we will show warning for
         * @param distance distance to mapPoint
         */
        void showWarningView(MapPoint mapPoint, int distance);

        /**
         * Hide warning view.
         */
        void hideWarningView();

        /**
         * Draw list of {@link MapPoint} on the map.
         *
         * @param cameras list of cameras to be drawn
         */
        void drawMarkersOnMap(List<MapPoint> cameras);

        /**
         * Focus map on specified {@link Location}
         */
        void focusMapToLocation(Location location);
    }

    interface Presenter extends BaseMvpPresenter<View> {

        /**
         * Start tracking user's location.
         */
        void startLocationTracking();

        /**
         * Stop stacking user's location.
         */
        void stopLocationTracking();

        /**
         * Event that will be called when user select specified {@link MapPoint}.
         */
        void tapOnMapMarker(MapPoint mapPoint);
    }
}
