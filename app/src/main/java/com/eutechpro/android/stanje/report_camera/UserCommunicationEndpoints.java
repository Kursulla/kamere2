package com.eutechpro.android.stanje.report_camera;

import java.util.ArrayList;

import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;import io.reactivex.Observable;

/**
 * API interface for communication user - developer
 */
interface UserCommunicationEndpoints {
    @FormUrlEncoded
    @POST("reportCamera")
    Observable<MessageResponse> reportCamera(@Field("email") String email, @Field("data") String message);

    @SuppressWarnings("unused")
    class MessageResponse {
        private String            message;
        private ArrayList<String> errors;

        public String getMessage() {
            return message;
        }

        public ArrayList<String> getErrors() {
            return errors;
        }

        @Override
        public String toString() {
            return "MessageResponse{" +
                    "message='" + message + '\'' +
                    ", errors=" + errors +
                    '}';
        }
    }

}
