package com.eutechpro.android.stanje.roadworks;

import com.eutechpro.android.stanje.R;
import com.eutechpro.android.stanje.data.entities.MapPoint;

import io.reactivex.android.schedulers.AndroidSchedulers;


class MvpPresenter implements Mvp.Presenter {
    private       Mvp.View  view;
    private       Mvp.Model model;

    MvpPresenter(Mvp.Model model) {
        this.model = model;
    }

    @Override
    public void bindView(Mvp.View view) {
        this.view = view;
        model.getAllRoadWorksRx()
                .observeOn(AndroidSchedulers.mainThread())
                .filter(mapPoints -> view != null)
                .subscribe(
                        mapPoints -> view.drawMarkersOnMap(mapPoints),
                        throwable -> view.showError(R.string.roadworks_error_unable_to_load_roadworks)
                );

    }

    @Override
    public void unBindView() {
        this.view = null;
    }

    @Override
    public void tapOnMapMarker(MapPoint mapPoint) {
        view.showBottomSheet(mapPoint);
    }
}
