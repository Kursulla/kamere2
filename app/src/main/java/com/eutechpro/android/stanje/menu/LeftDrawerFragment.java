package com.eutechpro.android.stanje.menu;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.eutechpro.android.stanje.BaseActivity;
import com.eutechpro.android.stanje.R;
import com.eutechpro.android.stanje.about.AboutActivity;
import com.eutechpro.android.stanje.border.BorderActivity;
import com.eutechpro.android.stanje.cameras.CamerasActivity;
import com.eutechpro.android.stanje.commons.RefreshTracker;
import com.eutechpro.android.stanje.fines.FinesActivity;
import com.eutechpro.android.stanje.firebase_tracking.FirebaseTracker;
import com.eutechpro.android.stanje.police.PolicePatrolsActivity;
import com.eutechpro.android.stanje.report_camera.ReportCameraActivity;
import com.eutechpro.android.stanje.roadworks.RoadworksActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LeftDrawerFragment extends Fragment {
    @BindView(R.id.menu_item_cameras)
    Button   camerasMenuItem;
    @BindView(R.id.menu_item_roadworks)
    Button   roadworksMenuItem;
    @BindView(R.id.menu_item_border)
    Button   borderMenuItem;
    @BindView(R.id.menu_item_report_camera)
    Button   reportCameraMenuItem;
    @BindView(R.id.menu_item_fines)
    Button   finesMenuItem;
    @BindView(R.id.menu_item_police_patrols)
    Button   policePatrolsMenuItem;
    @BindView(R.id.menu_item_about)
    Button   aboutMenuItem;
    @BindView(R.id.latest_refresh)
    TextView latestRefresh;
    private FirebaseTracker firebaseTracker;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.left_drawer_layout, container, false);
        ButterKnife.bind(this, view);
        initListeners();

        makeButtonSelected(getActivity());
        RefreshTracker refreshTracker = new RefreshTracker(getContext());
        if(refreshTracker.getRefreshTimestamp() != 0) {
            latestRefresh.setText(getString(R.string.left_drawer_latest_refresh, refreshTracker.getLatestRefreshTimeAsString()));
        }
        firebaseTracker = new FirebaseTracker(getContext());

        return view;
    }

    //region Private methods
    private void initListeners() {
        camerasMenuItem.setOnClickListener(view -> {
            firebaseTracker.logActivityOpenedFromDrawer(CamerasActivity.class);
            Intent intent = new Intent(getContext(), CamerasActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            startActivity(intent);
            if (getActivity() != null){
                ((BaseActivity) getActivity()).closeDrawer();
            }
        });
        roadworksMenuItem.setOnClickListener(view -> {
            firebaseTracker.logActivityOpenedFromDrawer(RoadworksActivity.class);
            Intent intent = new Intent(getContext(), RoadworksActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            startActivity(intent);
            if (getActivity() != null){
                ((BaseActivity) getActivity()).closeDrawer();
            }
        });
        borderMenuItem.setOnClickListener(v -> {
            firebaseTracker.logActivityOpenedFromDrawer(BorderActivity.class);
            Intent intent = new Intent(getContext(), BorderActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            startActivity(intent);
            if (getActivity() != null){
                ((BaseActivity) getActivity()).closeDrawer();
            }
        });
        reportCameraMenuItem.setOnClickListener(v -> {
            firebaseTracker.logActivityOpenedFromDrawer(ReportCameraActivity.class);
            Intent intent = new Intent(getContext(), ReportCameraActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            startActivity(intent);
            if (getActivity() != null){
                ((BaseActivity) getActivity()).closeDrawer();
            }
        });
        policePatrolsMenuItem.setOnClickListener(v -> {
            firebaseTracker.logActivityOpenedFromDrawer(PolicePatrolsActivity.class);
            Intent intent = new Intent(getContext(), PolicePatrolsActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            startActivity(intent);
            if (getActivity() != null){
                ((BaseActivity) getActivity()).closeDrawer();
            }
        });
        finesMenuItem.setOnClickListener(v -> {
            firebaseTracker.logActivityOpenedFromDrawer(FinesActivity.class);
            Intent intent = new Intent(getContext(), FinesActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            startActivity(intent);
            if (getActivity() != null){
                ((BaseActivity) getActivity()).closeDrawer();
            }
        });
        aboutMenuItem.setOnClickListener(v -> {
            firebaseTracker.logActivityOpenedFromDrawer(AboutActivity.class);
            Intent intent = new Intent(getContext(), AboutActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            startActivity(intent);
            if (getActivity() != null){
                ((BaseActivity) getActivity()).closeDrawer();
            }
        });
    }

    private void makeButtonSelected(Activity activity) {
        if(activity instanceof CamerasActivity) {
            camerasMenuItem.setBackgroundResource(R.color.colorPrimaryDark);
        } else if(activity instanceof BorderActivity) {
            borderMenuItem.setBackgroundResource(R.color.colorPrimaryDark);
        } else if(activity instanceof RoadworksActivity) {
            roadworksMenuItem.setBackgroundResource(R.color.colorPrimaryDark);
        }
    }
    //endregion
}
