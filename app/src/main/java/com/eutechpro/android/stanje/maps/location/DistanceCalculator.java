package com.eutechpro.android.stanje.maps.location;

import android.location.Location;
import android.util.Log;

import java.util.List;

import com.eutechpro.android.stanje.data.entities.MapPoint;

public class DistanceCalculator {
    private static final String TAG = "DistanceCalculator";
    private       MapPoint nearestOne;
    private       int      distanceToNearestOne;
    private final int      distanceThreshold;

    public DistanceCalculator(int distanceThreshold) {
        this.distanceThreshold = distanceThreshold;
    }

    public boolean findNearestCamera(Location usersLocation, List<MapPoint> cameras) {
        nearestOne = null;
        distanceToNearestOne = 9999999;
        for (MapPoint mapPoint : cameras) {
            long distance = calculateDistanceToCamera(usersLocation, mapPoint);

            if (distance < distanceToNearestOne && distance < distanceThreshold && distance != 0) {
                distanceToNearestOne = (int) distance;
                nearestOne = mapPoint;
            }
        }
        Log.d(TAG, "findNearestCamera: Shortest distance is " + distanceToNearestOne);
        return nearestOne != null;
    }

    public MapPoint getNearestCamera() {
        return nearestOne;
    }

    public int getDistanceToNearestCamera() {
        return distanceToNearestOne;
    }

    private long calculateDistanceToCamera(Location location, MapPoint mapPoint) {
        float[] allDistances = new float[1];
        Location.distanceBetween(location.getLatitude(), location.getLongitude(), mapPoint.getLatDouble(), mapPoint.getLngDouble(), allDistances);
        return (long) (allDistances[0]);
    }
}