package com.eutechpro.android.stanje.data.rest;

import android.support.design.BuildConfig;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;


public class RetrofitFactory {
    private static final String   BASE_URL_DEBUG = "https://kamere-py.herokuapp.com/";
    private static final String   BASE_URL_PROD  = "https://kamere-py.herokuapp.com/";
    private static       int      timeout        = 10;
    private static       TimeUnit timeoutUnit    = TimeUnit.MINUTES;
    private RetrofitFactory() {
        //to prevent init
    }

    public static Retrofit generateRetrofit() {
        if(BuildConfig.DEBUG){
            timeout = 5;
            timeoutUnit = TimeUnit.SECONDS;
            return generateRetrofit(BASE_URL_DEBUG);
        }else{
            return generateRetrofit(BASE_URL_PROD);
        }
    }

    public static Retrofit generateRetrofit(String baseUrl) {


        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create());

        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient.Builder httpClientBuilder = new OkHttpClient.Builder();
        httpClientBuilder.connectTimeout(timeout, timeoutUnit);
        httpClientBuilder.readTimeout(timeout, timeoutUnit);
        httpClientBuilder.writeTimeout(timeout, timeoutUnit);
        httpClientBuilder.addInterceptor(loggingInterceptor);
        httpClientBuilder.addInterceptor(
                chain -> {
                    Request request = chain
                            .request().newBuilder()
                            .addHeader("x-device-23", "90e7c28965e29ca7b7f1889fae849f7dc09b4f04023002b5d2210ab7c062c760")
                            .build();
                    return chain.proceed(request);
        });
        builder.client(httpClientBuilder.build());

        return builder.build();//Build retrofit
    }
}
