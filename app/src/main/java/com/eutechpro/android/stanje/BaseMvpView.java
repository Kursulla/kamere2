package com.eutechpro.android.stanje;

import android.support.annotation.StringRes;

public interface BaseMvpView {
    void showError(@StringRes int messageRes);
}
