package com.eutechpro.android.stanje.roadworks;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.eutechpro.android.stanje.commons.db.DbHelper;
import com.eutechpro.android.stanje.data.entities.MapPoint;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.schedulers.Schedulers;


/**
 * SqLite implementation of {@link RoadworksRepository}.
 */
public class RoadworksDbRepository implements RoadworksRepository {
    private static final String[] allColumns = {
            DbHelper.ROADWORKS_ID,
            DbHelper.ROADWORKS_TITLE,
            DbHelper.ROADWORKS_DESCRIPTION,
            DbHelper.ROADWORKS_META,
            DbHelper.ROADWORKS_TYPE,
            DbHelper.ROADWORKS_LAT,
            DbHelper.ROADWORKS_LNG,
    };
    private final SQLiteDatabase database;

    public RoadworksDbRepository(DbHelper dbHelper) {
        database = dbHelper.getWritableDatabase();
    }

    @Override
    public Observable<Boolean> saveRoadworkRx(final MapPoint roadworkToSave) {
        return Observable.create((ObservableOnSubscribe<Boolean>)
                subscriber -> {
                    subscriber.onNext(saveRoadwork(roadworkToSave));
                    subscriber.onComplete();
        }).subscribeOn(Schedulers.io());
    }

    @Override
    public Observable<Boolean> saveRoadworksInBulkRx(final List<MapPoint> mapPoints) {
        return Observable.create((ObservableOnSubscribe<Boolean>)
                subscriber -> {
                    boolean status = false;
                    for (MapPoint roadwork : mapPoints) {
                        status = saveRoadwork(roadwork);
                    }
                    subscriber.onNext(status);
                    subscriber.onComplete();
        }).subscribeOn(Schedulers.io());
    }

    @Override
    public Observable<List<MapPoint>> getRoadworksRx() {
        return Observable.create((ObservableOnSubscribe<List<MapPoint>>)
                subscriber -> {
                    subscriber.onNext(getRoadworks());
                    subscriber.onComplete();
        }).subscribeOn(Schedulers.io());
    }

    @Override
    public Observable<Boolean> removeAllRoadworksRx() {
        return Observable.create((ObservableOnSubscribe<Boolean>)
                subscriber -> {
                    database.delete(DbHelper.ROADWORKS_TABLE_NAME, "1", null);
                    subscriber.onNext(true);
                    subscriber.onComplete();
        }).subscribeOn(Schedulers.io());
    }

    @Override
    public Observable<Boolean> replaceAllRoadworks(List<MapPoint> roadworks) {
        return removeAllRoadworksRx()
                .flatMap(removed -> saveRoadworksInBulkRx(roadworks));
    }

    private boolean saveRoadwork(MapPoint mapPoints) {
        ContentValues contentValues = DbHelper.roadworksModelToContentValues(mapPoints);
        long          savedEntryId  = database.insertWithOnConflict(DbHelper.ROADWORKS_TABLE_NAME, null, contentValues, SQLiteDatabase.CONFLICT_REPLACE);
        return savedEntryId != -1;
    }

    private List<MapPoint> getRoadworks() {
        List<MapPoint> mapPoints = new ArrayList<>();
        Cursor         cursor    = null;
        try {
            cursor = database.query(DbHelper.ROADWORKS_TABLE_NAME, allColumns, null, null, null, null, null);

            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                MapPoint roadwork = DbHelper.cursorToRoadworksModel(cursor);
                mapPoints.add(roadwork);
                cursor.moveToNext();
            }
        } finally {
            if (cursor != null){
                cursor.close();
            }
        }
        return mapPoints;
    }

}
