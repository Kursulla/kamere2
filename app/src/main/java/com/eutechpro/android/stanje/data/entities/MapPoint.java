package com.eutechpro.android.stanje.data.entities;

import java.io.Serializable;

public class MapPoint implements Serializable {
    private long id;
    private String title;
    private String description;
    private String meta;
    private String type;
    private String image;
    private String lat;
    private String lng;

    public MapPoint() {
        super();
    }

    public MapPoint(long id, String title, String lat, String lng) {
        this.id = id;
        this.title = title;
        this.lat = lat;
        this.lng = lng;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getLat() {
        return lat;
    }

    public double getLatDouble() {
        return Double.parseDouble(lat);
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public double getLngDouble() {
        return Double.parseDouble(lng);
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getMeta() {
        return meta;
    }

    public void setMeta(String meta) {
        this.meta = meta;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String toString() {
        return "Point::toString\n" + title + "\n" + description + "\n" + image + "\n" + lat + "\n" + lng + "\n#####################################\n";
    }
}
