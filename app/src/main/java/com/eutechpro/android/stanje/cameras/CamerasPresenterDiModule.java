package com.eutechpro.android.stanje.cameras;

import android.content.Context;

import com.eutechpro.android.stanje.commons.db.DbHelper;
import com.eutechpro.android.stanje.maps.location.LocationManagerGApi;
import com.eutechpro.android.stanje.maps.location.SimpleLocationManager;

import dagger.Module;
import dagger.Provides;

@Module
public class CamerasPresenterDiModule {
    @Provides
    Mvp.Presenter provideCamerasActivityPresenter(Context context) {
        CamerasRepository     camerasRepository = new CamerasRepositoryDb(DbHelper.getDbHelperInstance(context));
        Mvp.Model             model             = new MvpModel(camerasRepository);
        SimpleLocationManager locationManager   = new LocationManagerGApi(context);
        return new MvpPresenter(model, locationManager);
    }
}
