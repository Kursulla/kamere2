package com.eutechpro.android.stanje.splash;


import com.eutechpro.android.stanje.BaseMvpPresenter;

import io.reactivex.Observable;


interface Mvp {
    interface Model{
        Observable<MvpModel.FetchingResult> dataStream();

        void refreshData();

        boolean isTimeForRefresh();
    }
    interface View {
        void showToast(int message);

        void proceedToNextScreen();
    }

    interface Presenter extends BaseMvpPresenter<View>{
        void startDataRefreshing();
    }
}
