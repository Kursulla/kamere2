package com.eutechpro.android.stanje;

import android.support.annotation.DrawableRes;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.View;

public abstract class BaseActivity extends AppCompatActivity {
    private Toolbar toolbar;
    private DrawerLayout drawerLayout;
    private OnDrawerAction onDrawerAction;

    //region Protected methods
    protected void initLeftDrawer() {
        drawerLayout = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle drawerListener = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.drawer_open, R.string.drawer_close) {
            @Override
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                invalidateOptionsMenu();
                onDrawerAction.drawerClosed();
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                invalidateOptionsMenu();
                onDrawerAction.drawerOpened();
            }
        };

        drawerLayout.addDrawerListener(drawerListener);
    }

    public void setOnDrawerAction(OnDrawerAction onDrawerAction) {
        this.onDrawerAction = onDrawerAction;
    }

    @SuppressWarnings("unused")
    protected void initToolBarWithLeftDrawer() {
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_menu_white_24dp);
        }
    }

    @SuppressWarnings("unused")
    protected void initToolBarWithLeftDrawer(@DrawableRes int iconResourceId) {
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_menu_white_24dp);
            getSupportActionBar().setIcon(iconResourceId);
        }
    }

    protected void initToolBarWithBack() {
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    @SuppressWarnings("unused")
    public void closeDrawer(){
        drawerLayout.closeDrawer(Gravity.START);
    }

    @SuppressWarnings("unused")
    public void openDrawer() {
        drawerLayout.openDrawer(Gravity.START);
    }
    //endregion

    public interface OnDrawerAction{
        void drawerOpened();
        void drawerClosed();
    }
}
