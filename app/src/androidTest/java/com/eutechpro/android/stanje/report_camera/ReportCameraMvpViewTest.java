package com.eutechpro.android.stanje.report_camera;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.StringRes;
import android.support.test.espresso.ViewInteraction;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.eutechpro.android.stanje.ApplicationDiComponent;
import com.eutechpro.android.stanje.ApplicationDiModule;
import com.eutechpro.android.stanje.DaggerApplicationDiComponent;
import com.eutechpro.android.stanje.DaggerTestRule;
import com.eutechpro.android.stanje.KamereApplication;
import com.eutechpro.android.stanje.R;
import com.eutechpro.android.stanje.firebase_tracking.FirebaseDiModule;
import com.eutechpro.android.stanje.firebase_tracking.FirebaseTracker;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import dagger.Module;
import dagger.Provides;
import io.reactivex.Observable;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.clearText;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.RootMatchers.withDecorView;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.core.Is.is;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(AndroidJUnit4.class)
public class ReportCameraMvpViewTest {
    @Rule
    public ActivityTestRule<ReportCameraActivity> activityTestRule = new DaggerTestRule<>(ReportCameraActivity.class, true, false, (application, activity) -> {

        ApplicationDiComponent appComponent = DaggerApplicationDiComponent
                .builder()
                .applicationDiModule(mock(ApplicationDiModule.class))
                .firebaseDiModule(new MockFirebaseDiModule(application.getApplicationContext()))
                .build();

        ReportCameraDiComponent component = DaggerReportCameraDiComponent
                .builder()
                .applicationDiComponent(appComponent)
                .reportCameraDiModule(new MockReportCameraDiModule())
                .build();
        KamereApplication.get(application).setReportCameraDiComponent(component);
    });

    private Mvp.Presenter presenter;
    private Mvp.View      view;
    private Mvp.Model     model;

    @Before
    public void setUp() throws Exception {
        model = mock(Mvp.Model.class);
        presenter = new MvpPresenter(model);


        view = activityTestRule.launchActivity(new Intent());
    }

    @Test
    public void test_validation_with_empty_email() throws Exception {
        onView(withId(R.id.send_btn)).perform(click());

        toastWithString(R.string.report_camera_wrong_email).check(matches(isDisplayed()));
    }

    @Test
    public void test_validation_with_bad_email() throws Exception {
        testWithBadEmail("somebademail.com");

        testWithBadEmail("someba@demail");
    }

    private void testWithBadEmail(String email) {
        onView(withId(R.id.email)).perform(clearText(), closeSoftKeyboard());
        onView(withId(R.id.email)).perform(typeText(email), closeSoftKeyboard());

        onView(withId(R.id.send_btn)).perform(click());

        toastWithString(R.string.report_camera_wrong_email).check(matches(isDisplayed()));
    }

    @Test
    public void test_validation_with_good_email() throws Exception {
        onView(withId(R.id.email)).perform(typeText("somegood@email.com"), closeSoftKeyboard());

        onView(withId(R.id.send_btn)).perform(click());

        toastWithString(R.string.report_camera_toast_empty_message).check(matches(isDisplayed()));
    }

    @Test
    public void test_mail_sent() throws Exception {
        when(model.reportCamera(anyString(), anyString())).thenReturn(Observable.just(true));

        onView(withId(R.id.email)).perform(typeText("somegood@email.com"), closeSoftKeyboard());
        onView(withId(R.id.message)).perform(typeText("some message"), closeSoftKeyboard());

        onView(withId(R.id.send_btn)).perform(click());

        toastWithString(R.string.report_camera_toast_message_sent).check(matches(isDisplayed()));

    }

    private ViewInteraction toastWithString(@StringRes int viewId) {
        return onView(withText(viewId)).inRoot(withDecorView(not(is(activityTestRule.getActivity().getWindow().getDecorView()))));
    }


    @Module
    public class MockReportCameraDiModule extends ReportCameraDiModule {
        @Provides
        public Mvp.Presenter providesPresenter() {
            return presenter;
        }
    }

    @Module
    public class MockFirebaseDiModule extends FirebaseDiModule {
        public MockFirebaseDiModule(Context context) {
            super(context);
        }

        @Provides
        FirebaseTracker providesFirebaseTracker() {
            return mock(FirebaseTracker.class);
        }
    }
}