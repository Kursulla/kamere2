package com.eutechpro.android.stanje;

import android.app.Activity;
import android.app.Application;
import android.support.annotation.NonNull;
import android.support.test.InstrumentationRegistry;
import android.support.test.rule.ActivityTestRule;

@SuppressWarnings("SameParameterValue")
public class DaggerTestRule<T extends Activity> extends ActivityTestRule<T> {
    private final OnBeforeActivityLaunchedListener<T> listener;

    public DaggerTestRule(Class<T> activityClass, OnBeforeActivityLaunchedListener<T> listener) {
        super(activityClass);
        this.listener = listener;
    }

    public DaggerTestRule(Class<T> activityClass, boolean initialTouchMode, OnBeforeActivityLaunchedListener<T> listener) {
        super(activityClass, initialTouchMode);
        this.listener = listener;
    }

    public DaggerTestRule(Class<T> activityClass, boolean initialTouchMode, boolean launchActivity, OnBeforeActivityLaunchedListener<T> listener) {
        super(activityClass, initialTouchMode, launchActivity);
        this.listener = listener;
    }

    @Override
    protected void beforeActivityLaunched() {
        super.beforeActivityLaunched();
        listener.beforeActivityLaunched((Application) InstrumentationRegistry
                .getInstrumentation()
                .getTargetContext()
                .getApplicationContext(), getActivity());
    }

    public interface OnBeforeActivityLaunchedListener<T> {
        @SuppressWarnings("UnusedParameters")
        void beforeActivityLaunched(@NonNull Application application, @NonNull T activity);
    }
}
